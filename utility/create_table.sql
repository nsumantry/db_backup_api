CREATE DATABASE e_tiket;


CREATE TABLE system_user( 
    id INT NOT NULL AUTO_INCREMENT, 
    nama VARCHAR(255) NOT NULL, 
    user_id VARCHAR(100) NOT NULL, 
    pswd VARCHAR(50) NOT NULL, 
    tipe VARCHAR(20) NOT NULL, 
    is_tambah_user VARCHAR(1) NOT NULL, 
    tgl_non_aktif DATE,
    PRIMARY KEY (id)
) ENGINE = INNODB;
//keterangan
//tipe = (admin = A / oprator = O)
//is_tambah_user = (T/F)

CREATE TABLE berita( 
    id INT NOT NULL AUTO_INCREMENT, 
    tanggal DATETIME NOT NULL, 
    kode VARCHAR(50) NOT NULL, 
    judul VARCHAR(500) NOT NULL, 
    isi LONGTEXT NOT NULL, 
    foto VARCHAR(500) NOT NULL,     
    user_id VARCHAR(100) NOT NULL,
    tgl_non_aktif DATE,
    PRIMARY KEY (id)
) ENGINE = INNODB;

CREATE TABLE master_pelanggan( 
    id INT NOT NULL AUTO_INCREMENT, 
    email VARCHAR(100) NOT NULL, 
    no_telpon VARCHAR(50) NOT NULL,
    tgl_lahir DATE  NOT NULL,
    pswd VARCHAR(50) NOT NULL,
    nama VARCHAR(255) NOT NULL,
    jenis_kelamin VARCHAR(1) NOT NULL,
    tipe_id VARCHAR(5) NOT NULL,
    nomor_id VARCHAR(255) NOT NULL,    
    user_id VARCHAR(100) NOT NULL,    
    tgl_aktif DATE,
    PRIMARY KEY (id)
) ENGINE = INNODB;
//keterangan
//jenis_kelamin = (Laki - laki = L / perempuan = P)
//tipe_id (KTP = K / Paspor = P / SIM = S / Lainnya = L)

CREATE TABLE penumpang(
    id INT NOT NULL AUTO_INCREMENT,
    id_pelanggan INT NOT NULL,
    nama VARCHAR(255) NOT NULL,
    jenis_kelamin VARCHAR(1) NOT NULL,
    tipe_id VARCHAR(5),
    nomor_id VARCHAR(255),
    jenis VARCHAR(255) NOT NULL,
    tgl_lahir DATE,    
    tgl_non_aktif DATE,
    PRIMARY KEY (id)
) ENGINE = INNODB;
//keterangan
//jenis_kelamin = (Laki - laki = L / perempuan = P)
//jenis = (Dewasa = D / bayi = B)
//tipe_id (KTP = K / Paspor = P / SIM = S / Lainnya = L)


CREATE TABLE master_kapal( 
    id INT NOT NULL AUTO_INCREMENT,
    kode VARCHAR(255) NOT NULL,
    nama VARCHAR(255) NOT NULL,
    jenis VARCHAR(100) NOT NULL,
    kapasitas INTEGER NOT NULL,
    user_id VARCHAR(100) NOT NULL,
    tgl_non_aktif DATE,
    PRIMARY KEY (id)
) ENGINE = INNODB;
//keterangan
//jenis = (Jalur utama = U / antar pulau = P)


CREATE TABLE master_pelabuhan( 
    id INT NOT NULL AUTO_INCREMENT, 
    kode VARCHAR(50) NOT NULL, 
    nama VARCHAR(255) NOT NULL,
    jenis VARCHAR(100) NOT NULL,
    user_id VARCHAR(100) NOT NULL,
    tgl_non_aktif DATE,
    PRIMARY KEY (id)
) ENGINE = INNODB;
//keterangan
//jenis = (Jalur utama = U / Antar pulau = P)

CREATE TABLE master_bayar( 
    id INT NOT NULL AUTO_INCREMENT, 
    kode VARCHAR(50) NOT NULL, 
    nama VARCHAR(255) NOT NULL,
    jenis VARCHAR(100) NOT NULL,
    user_id VARCHAR(100) NOT NULL,
    tgl_non_aktif DATE,
    PRIMARY KEY (id)
) ENGINE = INNODB;
//keterangan
//jenis = (Grai retail = R / Kartu debet debet online = D / ATM = A)


CREATE TABLE wisata( 
    id INT NOT NULL AUTO_INCREMENT, 
    tanggal DATETIME NOT NULL,
    kode VARCHAR(50) NOT NULL, 
    judul VARCHAR(500) NOT NULL,
    nama_pulau VARCHAR(500) NOT NULL,
    fasilitas LONGTEXT NOT NULL,    
    foto_1 VARCHAR(500) NOT NULL,
    foto_2 VARCHAR(500) NOT NULL,
    foto_3 VARCHAR(500) NOT NULL,
    user_id VARCHAR(100) NOT NULL,
    tgl_non_aktif DATE,
    PRIMARY KEY (id)
) ENGINE = INNODB;

CREATE TABLE token_aktivasi( 
    id INT NOT NULL AUTO_INCREMENT,
    id_pelanggan INT NOT NULL,
    token VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
) ENGINE = INNODB;


CREATE TABLE jadwal_kapal( 
    id INT NOT NULL AUTO_INCREMENT,
    tgl_berangkat DATETIME NOT NULL,
    tgl_sampai DATETIME NOT NULL,
    id_kapal INT NOT NULL,
    id_pelabuhan_asal INT NOT NULL,
    id_pelabuhan_tujuan INT NOT NULL,
    harga_dewasa DOUBLE NOT NULL,
    harga_anak DOUBLE NOT NULL,
    jumlah_penumpang INT NOT NULL,
    tgl_input DATETIME NOT NULL,
    user_id VARCHAR(100) NOT NULL,
    PRIMARY KEY (id)
) ENGINE = INNODB;

CREATE TABLE jadwal_kapal_detail(
    id INT NOT NULL AUTO_INCREMENT,
    id_jadwal INT NOT NULL,
    id_jadwal_transit INT NOT NULL,
    PRIMARY KEY (id)
) ENGINE = INNODB;

CREATE TABLE pesanan( 
    id INT NOT NULL AUTO_INCREMENT,
    tanggal DATE NOT NULL,
    nomor VARCHAR(100) NOT NULL,
    nama VARCHAR(255) NOT NULL,
    no_tiket VARCHAR(100),
    id_pelanggan INT NOT NULL,
    id_jadwal INT NOT NULL,
    harga_dewasa DOUBLE NOT NULL,
    harga_anak DOUBLE NOT NULL,
    jumlah_dewasa INT NOT NULL,
    jumlah_anak INT,
    total_harga DOUBLE NOT NULL,
    id_bayar INT,
    tgl_bayar DATETIME,
    tgl_expaired DATETIME,    
    PRIMARY KEY (id)
) ENGINE = INNODB;


CREATE TABLE pesanan_detail( 
    id INT NOT NULL AUTO_INCREMENT,
    id_pesanan INT NOT NULL,
    id_penumpang INT NOT NULL,
    nama VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
) ENGINE = INNODB;
//keterangan format  {"0":10,"1":13,"2":15}

//insert data
INSERT INTO master_pelanggan (id, email, no_telpon, tgl_lahir, pswd, nama, jenis_kelamin, tipe_id, nomor_id, user_id, tgl_aktif)
VALUES(1, '-', '-', NOW(), '', '', '', '', '-', '', NOW());

INSERT INTO penumpang (id, id_pelanggan, nama, jenis_kelamin, tipe_id, nomor_id, jenis, tgl_lahir, tgl_non_aktif  )
VALUES(1, 1, '', '', '', '-', '', NOW(), NULL);

INSERT INTO master_bayar VALUES(1,'LSG','Bayar langsung','L','Admin', NULL)