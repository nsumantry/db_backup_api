<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* tiket-pesanan.html */
class __TwigTemplate_ba9b630f30a790201a89fdec81353ecdd1906709b95f414fa8a02ce673234ea1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<html>
    <head>
        <title>Tiket Elektronik</title> 
        <link rel=\"stylesheet\" href=\"/e_tiket_api/static/assets/css/printa4.css\">
        
    </head>
    <body>
        hello ";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["data"] ?? null), "id", [], "any", false, false, false, 8), "html", null, true);
        echo "
    </body>

</html>";
    }

    public function getTemplateName()
    {
        return "tiket-pesanan.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 8,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "tiket-pesanan.html", "C:\\xampp\\htdocs\\e_tiket_api\\public\\twig\\templates\\tiket-pesanan.html");
    }
}
