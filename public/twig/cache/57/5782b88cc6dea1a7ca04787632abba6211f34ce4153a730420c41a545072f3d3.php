<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* tiket.html */
class __TwigTemplate_06ff10793c1918c7ea385df4505eea233bd72500abbf27bfe0b4c3938aa9c4d2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<html>
    <head>
        <title>Tiket Elektronik</title> 
        <link rel=\"stylesheet\" href=\"/e_tiket_api/static/assets/css/printa4.css\">
        
    </head>
    <body>
        hello ";
        // line 8
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "
    </body>

</html>";
    }

    public function getTemplateName()
    {
        return "tiket.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 8,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "tiket.html", "C:\\xampp\\htdocs\\e_tiket_api\\public\\twig\\templates\\tiket.html");
    }
}
