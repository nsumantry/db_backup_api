<?php
use Slim\Http\Request;
use Slim\Http\Response;
require_once __DIR__ . '/../../src/general/function_general.php';
require_once __DIR__ . '/../../src/general/const_global.php';


$app->post('/master_pelabuhan/save', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

        $kode    = $dtPost['kode'];
        $nama    = $dtPost['nama'];
        $jenis   = $dtPost['jenis'];        
		$user_id = $dtPost['user_id'];
		
		if (get_count($db, "master_pelabuhan","(UPPER(kode) = UPPER('$kode'))") > 0) {
            return $response->withJson(setInfo(STATUS_GAGAL, "Kode sudah ada", $id), 200);
        }
        
        $sql =  "INSERT INTO master_pelabuhan (kode, nama, jenis, user_id) ".
                "VALUES(:kode, :nama, :jenis, :user_id)";
		$query = $db->prepare($sql);
		$query->bindParam(':kode', $kode);
		$query->bindParam(':nama', $nama);
		$query->bindParam(':jenis', $jenis);		
        $query->bindParam(':user_id', $user_id);        
		$query->execute();       
		$id = $db->lastInsertId();
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_SIMPAN, $id), 200);   
});

$app->post('/master_pelabuhan/edit', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

		$id 	 = $dtPost['id'];
        $kode    = $dtPost['kode'];
        $nama    = $dtPost['nama'];
        $jenis   = $dtPost['jenis'];        
        $user_id = $dtPost['user_id'];
		
		$sql =  "UPDATE master_pelabuhan SET ".
					"kode = :kode,".
					"nama = :nama,".
					"jenis = :jenis,".					
					"user_id= :user_id ".
				"WHERE id = :id";
		$query = $db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':kode', $kode);
		$query->bindParam(':nama', $nama);
		$query->bindParam(':jenis', $jenis);		
        $query->bindParam(':user_id', $user_id);        
		$query->execute();       
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_UBAH, $id), 200);   
});

$app->post('/master_pelabuhan/aktivasi', function (Request $request, Response $response) {	
	$db = $this->db;
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

		$id 	 	   = $dtPost['id'];
		$tgl_non_aktif = $dtPost['tgl_non_aktif'];        
		if ($tgl_non_aktif == ""){
			$tgl_non_aktif = null;
		}
		
		$sql =  "UPDATE master_pelabuhan SET ".					
					"tgl_non_aktif= :tgl_non_aktif ".
				"WHERE id = :id";
		$query = $db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':tgl_non_aktif', $tgl_non_aktif);		
		$query->execute();       
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_AKTIVASI, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_AKTIVASI, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_AKTIVASI, $id), 200);   
});


$app->get('/master_pelabuhan/load', function (Request $request, Response $response, array $args) {
    $db = $this->db;	
    $jenis    = $request->getQueryParam("jenis");
	$is_aktif = $request->getQueryParam("is_aktif");
	$cari     = $request->getQueryParam("cari");
	$orderBy  = $request->getQueryParam("order_by");
	$limit    = $request->getQueryParam("limit");
	$offset   = $request->getQueryParam("offset");

	$filter  = "";
	$filter2 = "";

	if (!empty($is_aktif)){
		if ($is_aktif == "T"){
			$filter  .= " AND tgl_non_aktif IS NULL ";		
		}else if ($is_aktif == "F"){
			$filter  .= " AND tgl_non_aktif IS NOT NULL ";			
		}
    }
    
    if (!empty($jenis)){
		$filter  .= " AND jenis = '$jenis' ";
	}

	if (!empty($cari)){
		$filter  .= " AND nama LIKE '%$cari%' ";
	}

	if (!empty($orderBy)){
		$filter2 .= " ORDER BY $orderBy ";
	}

	if (!empty($limit)){
		$filter2 .= " LIMIT $limit ";
	}

	if (!empty($offset)){
		$filter2 .= " OFFSET $offset ";
	}	
	
	$data  = [];
	$hasil = [];
	try {		
		$jml_data = get_count($db, "master_pelabuhan"," id <> 0 $filter");

		$sql = 	"SELECT id, kode, nama, jenis, user_id, tgl_non_aktif, $jml_data AS jml_data ".
                "FROM master_pelabuhan WHERE id <> 0 $filter $filter2 ";
		$query = $db->prepare($sql); 
		$result = $query->execute();		
		
		if ($result) {		
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_SUKSES, $data);		
		}else{
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_GAGAL, $data);		
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}		
  	return $response->withJson($hasil);
});

$app->get('/master_pelabuhan/get', function (Request $request, Response $response, array $args) {
	$db = $this->db;
	$id = $request->getQueryParam("id");

	try {
		$sql = 	"SELECT id, kode, nama, jenis, user_id, tgl_non_aktif ".
				"FROM master_pelabuhan ".
				"WHERE id = $id ";
		$query = $db->prepare($sql); 
		$result = $query->execute();
		$hasil = [];
		if ($result) {		
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_SUKSES, $data);		
		}else{
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_GAGAL, $data);		
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil);
});