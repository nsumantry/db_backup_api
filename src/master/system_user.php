<?php
use Slim\Http\Request;
use Slim\Http\Response;
require_once __DIR__ . '/../../src/general/function_general.php';
require_once __DIR__ . '/../../src/general/const_global.php';
require_once __DIR__ . '/../../src/general/engine_general.php';


$app->post('/system_user/save', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
		$dtPost = $request->getParsedBody();

        $nama           = $dtPost['nama'];
        $user_id        = $dtPost['user_id'];
        $pswd           = $dtPost['pswd'];        
        $tipe           = $dtPost['tipe'];
        $is_tambah_user = $dtPost['is_tambah_user'];        

        if (get_count($db, "system_user","(UPPER(user_id) = UPPER('$user_id'))") > 0) {
            return $response->withJson(setInfo(STATUS_GAGAL, "User id sudah terdaftar", $id), 200);
        }
        
        $sql =  "INSERT INTO system_user (nama, user_id, pswd, tipe, is_tambah_user) ".
                "VALUES(:nama, :user_id, :pswd, :tipe, :is_tambah_user)";
		$query = $db->prepare($sql);
		$query->bindParam(':nama', $nama);
        $query->bindParam(':user_id', $user_id);
        $query->bindParam(':pswd', $pswd);
        $query->bindParam(':tipe', $tipe);
        $query->bindParam(':is_tambah_user', $is_tambah_user);
		$query->execute();       
		$id = $db->lastInsertId();
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_SIMPAN, $id), 200);   
});

$app->post('/system_user/edit', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

        $id             = $dtPost['id'];
		$nama           = $dtPost['nama'];        
        $tipe           = $dtPost['tipe'];
        $is_tambah_user = $dtPost['is_tambah_user'];
		
		$sql =  "UPDATE system_user SET ".
					"nama = :nama,".
					"tipe = :tipe,".
					"is_tambah_user = :is_tambah_user ".					
				"WHERE id = :id";
		$query = $db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':nama', $nama);
		$query->bindParam(':tipe', $tipe);
		$query->bindParam(':is_tambah_user', $is_tambah_user);		
		$query->execute();       
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_UBAH, $id), 200);   
});

$app->post('/system_user/aktivasi', function (Request $request, Response $response) {	
	$db = $this->db;
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

		$id 	 	   = $dtPost['id'];
		$tgl_non_aktif = $dtPost['tgl_non_aktif'];        
		if ($tgl_non_aktif == ""){
			$tgl_non_aktif = null;
		}
		
		$sql =  "UPDATE system_user SET ".					
					"tgl_non_aktif= :tgl_non_aktif ".
				"WHERE id = :id";
		$query = $db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':tgl_non_aktif', $tgl_non_aktif);		
		$query->execute();       
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_AKTIVASI, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_AKTIVASI, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_AKTIVASI, $id), 200);   
});


$app->get('/system_user/load', function (Request $request, Response $response, array $args) {    
    $db = $this->db;	
    $is_aktif = $request->getQueryParam("is_aktif");
	$orderBy  = $request->getQueryParam("order_by");
	$limit    = $request->getQueryParam("limit");
	$offset   = $request->getQueryParam("offset");

	$filter  = "";
    $filter2 = "";
    
    if (!empty($is_aktif)){
		if ($is_aktif == "T"){
			$filter  .= " AND tgl_non_aktif IS NULL ";		
		}else if ($is_aktif == "F"){
			$filter  .= " AND tgl_non_aktif IS NOT NULL ";			
		}
	}

	if (!empty($orderBy)){
		$filter2 .= " ORDER BY $orderBy ";
	}

	if (!empty($limit)){
		$filter2 .= " LIMIT $limit ";
	}

	if (!empty($offset)){
		$filter2 .= " OFFSET $offset ";
	}

    $data  = [];
	$hasil = [];
	try {
        $jml_data = get_count($db, "system_user"," id <> 0 $filter");

		$sql = 	"SELECT id, nama, user_id, pswd, tipe, is_tambah_user, tgl_non_aktif, $jml_data AS jml_data ".
                "FROM system_user WHERE id <> 0 $filter $filter2 ";                
		$query = $db->prepare($sql); 
		$result = $query->execute();
		
		if ($result) {		
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_SUKSES, $data);		
		}else{
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_GAGAL, $data);		
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}		
  	return $response->withJson($hasil);
});

$app->get('/system_user/get', function (Request $request, Response $response, array $args) {
	$db = $this->db;	
	$id = $request->getQueryParam("id");

	try {
		$sql = 	"SELECT id, nama, user_id, pswd, tipe, is_tambah_user, tgl_non_aktif ".
                "FROM system_user ".
                "WHERE id = $id ";
		$query = $db->prepare($sql); 
		$result = $query->execute();
		$hasil = [];
		if ($result) {		
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_SUKSES, $data);
		}else{
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_GAGAL, $data);		
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil);
});


$app->get('/system_user/masuk', function (Request $request, Response $response, array $args) {
	$db = $this->db;	
	$id = 0;
    
    $user_id  = $request->getQueryParam("user_id");
    $password = $request->getQueryParam("password");

    if (empty($user_id) && ($user_id == "")){ 
        return $response->withJson(setInfo(STATUS_GAGAL, "User belum diisi", $id), 200);  
    }
    
    if (empty($password) && ($password == "")){ 
        return $response->withJson(setInfo(STATUS_GAGAL, "Password belum diisi", $id), 200);  
	}
    
	try {
        if (get_count($db, "system_user","(UPPER(user_id) = UPPER('$user_id')) AND tgl_non_aktif IS NULL") <= 0) {
            return $response->withJson(setInfo(STATUS_GAGAL, "User id belum terdaftar", $id), 200);
        }

		$sql = 	"SELECT id, nama, user_id, pswd, tipe, is_tambah_user, tgl_non_aktif ".
                "FROM system_user ".
                "WHERE UPPER(user_id) = UPPER('$user_id') AND pswd = '$password' AND tgl_non_aktif IS NULL";                
		$query = $db->prepare($sql); 
        $query->execute();
        $rowCount = $query->rowCount();
        if ($rowCount > 0){                  
            $hasil = [];
            $data = $query->fetchAll();
            $hasil = setHasil(STATUS_SUKSES, $data);
            return $response->withJson($hasil);
        }else{
            return $response->withJson(setInfo(STATUS_GAGAL, "User id atau password salah", $id), 200);
        }
	} catch(PDOException $pdoe) {
		return $response->withJson(setInfo(PESAN_GAGAL_KESALAHAN, "", $id), 200);
	}
});


$app->post('/system_user/ubah_password', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

        $id         = $dtPost['id'];        
        $pswd_lama  = $dtPost['pswd_lama'];
        $pswd_baru  = $dtPost['pswd_baru'];        
        
        if (empty($pswd_lama) && ($pswd_lama == "")){ 
            return $response->withJson(setInfo(STATUS_GAGAL, "Password lama belum diisi", $id), 200);
        }

        if (empty($pswd_baru) && ($pswd_baru == "")){ 
            return $response->withJson(setInfo(STATUS_GAGAL, "Password baru belum diisi", $id), 200);
        }

        if (get_count($db, "system_user","pswd = '$pswd_lama' AND id = $id") <= 0) {
            return $response->withJson(setInfo(STATUS_GAGAL, "Password lama tidak sesuai", $id), 200);
        }
		
		$sql =  "UPDATE system_user SET ".
					"pswd = :pswd ".					
				"WHERE id = :id";
		$query = $db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':pswd', $pswd_baru);
        $result = $query->execute();        
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_UBAH, $id), 200);   
});