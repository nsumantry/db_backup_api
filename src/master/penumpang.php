<?php
use Slim\Http\Request;
use Slim\Http\Response;
require_once __DIR__ . '/../../src/general/function_general.php';
require_once __DIR__ . '/../../src/general/const_global.php';
require_once __DIR__ . '/../../src/general/engine_general.php';


$app->post('/penumpang/save', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

		$id_pelanggan  = $dtPost['id_pelanggan'];
		$nama          = $dtPost['nama'];
		$jenis_kelamin = $dtPost['jenis_kelamin'];
        $tipe_id       = $dtPost['tipe_id'];
		$nomor_id      = $dtPost['nomor_id'];
		$jenis         = $dtPost['jenis'];
		$tgl_lahir     = $dtPost['tgl_lahir'];
		if ($tgl_lahir == ""){
			$tgl_lahir = "null";
		}else if ($tgl_lahir != "null"){			
			$tgl_lahir = "'$tgl_lahir'";
		}
        
        $sql =  "INSERT INTO penumpang (id_pelanggan, nama, jenis_kelamin, tipe_id, nomor_id, jenis, tgl_lahir) ".
                "VALUES(:id_pelanggan, :nama, :jenis_kelamin, :tipe_id, :nomor_id, :jenis, $tgl_lahir)";
		$query = $db->prepare($sql);
		$query->bindParam(':id_pelanggan', $id_pelanggan);        
		$query->bindParam(':nama', $nama);
		$query->bindParam(':jenis_kelamin', $jenis_kelamin);
        $query->bindParam(':tipe_id', $tipe_id);
		$query->bindParam(':nomor_id', $nomor_id);
		$query->bindParam(':jenis', $jenis);
		// $query->bindParam(':tgl_lahir', $tgl_lahir);		
		
		$query->execute();       
		$id = $db->lastInsertId();		
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_SIMPAN, $id), 200);   
});

$app->post('/penumpang/edit', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
		$dtPost = $request->getParsedBody();
		$id  		   = $dtPost['id'];
		$id_pelanggan  = $dtPost['id_pelanggan'];
		$nama          = $dtPost['nama'];
		$jenis_kelamin = $dtPost['jenis_kelamin'];
        $tipe_id       = $dtPost['tipe_id'];
		$nomor_id      = $dtPost['nomor_id'];
		$jenis         = $dtPost['jenis'];
		$tgl_lahir     = $dtPost['tgl_lahir'];
		if ($tgl_lahir == ""){
			$tgl_lahir = "null";
		}else if ($tgl_lahir != "null"){			
			$tgl_lahir = "'$tgl_lahir'";
		}
		
		$sql =  "UPDATE penumpang SET ".
					"id_pelanggan = :id_pelanggan,".
					"nama = :nama,".
					"jenis_kelamin = :jenis_kelamin,".
					"tipe_id = :tipe_id,".
					"nomor_id = :nomor_id,".
					"jenis = :jenis,".
					"tgl_lahir= $tgl_lahir ".
				"WHERE id = :id";
		$query = $db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':id_pelanggan', $id_pelanggan);
		$query->bindParam(':nama', $nama);
		$query->bindParam(':jenis_kelamin', $jenis_kelamin);
		$query->bindParam(':tipe_id', $tipe_id);		
		$query->bindParam(':nomor_id', $nomor_id);        
		$query->bindParam(':jenis', $jenis);        
		//$query->bindParam(':tgl_lahir', $tgl_lahir);        
		$query->execute();       
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_UBAH, $id), 200);   
});	

$app->post('/penumpang/aktivasi', function (Request $request, Response $response) {	
	$db = $this->db;
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

		$id 	 	   = $dtPost['id'];
		$tgl_non_aktif = $dtPost['tgl_non_aktif'];        
		if ($tgl_non_aktif == ""){
			$tgl_non_aktif = null;
		}
		
		$sql =  "UPDATE penumpang SET ".					
					"tgl_non_aktif= :tgl_non_aktif ".
				"WHERE id = :id";
		$query = $db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':tgl_non_aktif', $tgl_non_aktif);		
		$query->execute();       
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_AKTIVASI, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_AKTIVASI, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_AKTIVASI, $id), 200);   
});


$app->get('/penumpang/get', function (Request $request, Response $response, array $args) {
	$db = $this->db;	
	$id			  = $request->getQueryParam("id"); //opsional
	$id_pelanggan = $request->getQueryParam("id_pelanggan"); //optional

	 $filter = "";

	if (!empty($id)){
		$filter .= " AND id = $id ";
	}

	if (!empty($id_pelanggan)){
		$filter .= " AND id_pelanggan = $id_pelanggan ";
	}	
	

	try {
		$sql = 	"SELECT id, id_pelanggan, nama, jenis_kelamin, tipe_id, nomor_id, jenis, DATE_FORMAT(tgl_lahir, '%d-%m-%Y') AS tgl_lahir, tgl_non_aktif ".
                "FROM penumpang ".
				"WHERE id <> 0 $filter ";				
		$query = $db->prepare($sql); 
		$result = $query->execute();
		$hasil = [];
		if ($result) {		
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_SUKSES, $data);
		}else{
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_GAGAL, $data);		
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil);
});