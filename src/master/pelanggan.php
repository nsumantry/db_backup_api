<?php
use Slim\Http\Request;
use Slim\Http\Response;
use Zend\Mail;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;

require_once __DIR__ . '/../../src/general/function_general.php';
require_once __DIR__ . '/../../src/general/const_global.php';
require_once __DIR__ . '/../../src/general/engine_general.php';

$container = $app->getContainer();
$container['mail_options'] = new SmtpOptions(array(
    'name'              => 'plesk-9.idweb.host',
    'host'              => 'plesk-9.idweb.host',
    'port'              => '465',
    'connection_class'  => 'login',
    'connection_config' => array(
        'username' => 'noreply@etiketpulauseribudishub.com',
        'password' => 'Qea1y#72',
        'ssl'      => 'ssl',
    ),
));

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('twig/templates', [
        'cache' => 'twig/cache'
    ]);

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

    return $view;
};

$app->post('/pelanggan/save', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

        $email         = $dtPost['email'];
		$no_telpon     = $dtPost['no_telpon'];
		$tgl_lahir     = $dtPost['tgl_lahir'];
        $pswd          = $dtPost['pswd'];
        $nama          = $dtPost['nama'];
        $jenis_kelamin = $dtPost['jenis_kelamin'];
        $tipe_id       = $dtPost['tipe_id'];
        $nomor_id      = $dtPost['nomor_id'];
        //$user_id       = $dtPost['user_id'];

        if (get_count($db, "master_pelanggan","(UPPER(email) = UPPER('$email'))") > 0) {
            return $response->withJson(setInfo(STATUS_GAGAL, "Email sudah terdaftar", $id), 200);
        }

        if (get_count($db, "master_pelanggan","(UPPER(no_telpon) = UPPER('$no_telpon'))") > 0) {
            return $response->withJson(setInfo(STATUS_GAGAL, "No. Telpon terdaftar", $id), 200);
        }

        // if (get_count($db, "master_pelanggan","(UPPER(user_id) = UPPER('$user_id'))") > 0) {
        //     return $response->withJson(setInfo(STATUS_GAGAL, "User id sudah terdaftar", $id), 200);
        // }        
        
        $sql =  "INSERT INTO master_pelanggan (email, no_telpon, tgl_lahir, pswd, nama, jenis_kelamin, tipe_id, nomor_id) ".
                "VALUES(:email, :no_telpon, :tgl_lahir, :pswd, :nama, :jenis_kelamin, :tipe_id, :nomor_id)";
		$query = $db->prepare($sql);
		$query->bindParam(':email', $email);
		$query->bindParam(':no_telpon', $no_telpon);
		$query->bindParam(':tgl_lahir', $tgl_lahir);
        $query->bindParam(':pswd', $pswd);
        $query->bindParam(':nama', $nama);
        $query->bindParam(':jenis_kelamin', $jenis_kelamin);
        $query->bindParam(':tipe_id', $tipe_id);
        $query->bindParam(':nomor_id', $nomor_id);
		$query->execute();       
		$id = $db->lastInsertId();
		$idPelanggan = $id;

		$token = RandomToken(32);
		$token = substr($token, 0, 31);
		
		$sql =  "INSERT INTO token_aktivasi (id_pelanggan, token) ".
                "VALUES(:id_pelanggan, :token)";
		$query = $db->prepare($sql);
		$query->bindParam(':id_pelanggan', $id);
        $query->bindParam(':token', $token);
		$query->execute();       
		$id_token = $db->lastInsertId();
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);  
	}

	send_mail_confirmation($this->get('mail_options'),$email,$token,$idPelanggan);
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_SIMPAN, $id), 200);   
});
function send_mail_confirmation(SmtpOptions $options,$email,$token,$idPelanggan){
	try {
		$transport = new SmtpTransport();
		
		$transport->setOptions($options);
		
		$mail = new Mail\Message();
		$str = "<h1>E-Tiket Confirmation Mail.</h1>";
		$str .= "<p>this is confirmation email, please follow link below to confirm your email</p>";
		$str .= "<button onclick=\"window.location.href='http://etiketpulauseribudishub.com/e_tiket_api/public/token_aktivasi/aktivasi?token=$token&id_pelanggan=$idPelanggan';\">Confirm Email</button>"; 
		$str .= "<p>if link above doesn't work, please copy link below and paste on browser address bar to confirm email</p>";
		$str .= "<p>http://etiketpulauseribudishub.com/e_tiket_api/public/token_aktivasi/aktivasi?token=$token&id_pelanggan=$idPelanggan</p>";
		
		$html = new MimePart($str);
		$html->type = "text/html";
		$body = new MimeMessage();
		$body->setParts(array($html));
	
		$mail->setBody($body);
		$mail->setFrom('noreply@etiketpulauseribudishub.com', 'E-Tiket Pulau Seribu Dishub');
		// $mail->addTo('mmrsgsw@gmail.com', 'Matthew');
		$mail->addTo($email);
		$mail->setSubject('E-Tiket Confirmation Mail.');

		$transport->send($mail);
	} catch(Exception $e) {
		return false;
	}
	return true;
}
function send_mail_reset_link(SmtpOptions $options,$email,$token,$idPelanggan){
	try {
		$transport = new SmtpTransport();
		
		$transport->setOptions($options);
		
		$mail = new Mail\Message();
		$str = "<h1>E-Tiket - Password Reset.</h1>";
		$str .= "<p>this is Passworde Reset confirmation email, please follow link below to reset your password</p>";
		$str .= "<button onclick=\"window.location.href='http://etiketpulauseribudishub.com/reset_password_form/$token/$idPelanggan';\">Reset Password</button>"; 
		$str .= "<p>if link above doesn't work, please copy link below and paste on browser address bar to confirm email</p>";
		$str .= "<p>http://etiketpulauseribudishub.com/reset_password_form/$token/$idPelanggan</p>";
		
		$html = new MimePart($str);
		$html->type = "text/html";
		$body = new MimeMessage();
		$body->setParts(array($html));
	
		$mail->setBody($body);
		$mail->setFrom('noreply@etiketpulauseribudishub.com', 'E-Tiket Pulau Seribu Dishub');
		// $mail->addTo('mmrsgsw@gmail.com', 'Matthew');
		$mail->addTo($email);
		$mail->setSubject('E-Tiket Password Reset.');

		$transport->send($mail);
	} catch(Exception $e) {
		return false;
	}
	return true;
}
$app->get('/pelanggan/send_mail_confirmation', function (Request $request, Response $response) {
	if (send_mail_confirmation($this->get('mail_options'),'mmrsgsw@gmail.com','1238964986892yb9iuhboi16237b',1)){
		return $response->withJson(setInfo(STATUS_SUKSES,'',''));
	}
	return $response->withJson(setInfo(STATUS_GAGAL,'',''));
});
$app->get('/pelanggan/download_tiket', function (Request $request, Response $response) {
	$db = $this->db;	
	$id = $request->getQueryParam("id");

	try {
		$sql = 	"SELECT id, email, no_telpon, pswd, nama, jenis_kelamin, tipe_id, nomor_id, user_id, tgl_aktif ".
                "FROM master_pelanggan ".
                "WHERE id = $id ";
		$query = $db->prepare($sql); 
		$result = $query->execute();
		$hasil = [];
		$data = null;
		if ($result) {		
			$data = $query->fetchAll();
		}	
	} catch(PDOException $pdoe) {
		return $response->withJson(setInfo(STATUS_GAGAL,$pdoe,''));
	}
	if ($data){
		$d = $data[0];
		$mpdf = new \Mpdf\Mpdf();
		// $mpdf->WriteHTML("<div>Section ".$d['email']." text</div>");
		$view = $this->view->render($response, 'tiket.html', [
			'name' => $d['email']
		]);
		$mpdf->WriteHTML($view);

		
		$response = $this->response->withHeader( 'Content-type', 'application/pdf' );
		$response->write( $mpdf->Output() );
		
		return $response;
	}
	
});
$app->get('/hello/{name}', function ($request, $response, $args) {
    return $this->view->render($response, 'tiket.html', [
        'name' => $args['name']
    ]);
})->setName('profile');

$app->post('/pelanggan/edit', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

		$id            = $dtPost['id'];
        $email         = $dtPost['email'];
		$no_telpon     = $dtPost['no_telpon'];
		$tgl_lahir     = $dtPost['tgl_lahir'];
        $nama          = $dtPost['nama'];
        $jenis_kelamin = $dtPost['jenis_kelamin'];
        $tipe_id       = $dtPost['tipe_id'];
        $nomor_id      = $dtPost['nomor_id'];        
        
        $sql =  "UPDATE master_pelanggan SET ".
					"email = :email, ".
					"no_telpon = :no_telpon, ".
					"tgl_lahir = :tgl_lahir, ".
					"nama = :nama, ".
					"jenis_kelamin = :jenis_kelamin, ".
					"tipe_id = :tipe_id, ".
					"nomor_id = :nomor_id ".
				"WHERE id = :id";

		$query = $db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':email', $email);
		$query->bindParam(':no_telpon', $no_telpon);
		$query->bindParam(':tgl_lahir', $tgl_lahir);        
        $query->bindParam(':nama', $nama);
        $query->bindParam(':jenis_kelamin', $jenis_kelamin);
        $query->bindParam(':tipe_id', $tipe_id);
        $query->bindParam(':nomor_id', $nomor_id);
		$query->execute();
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_UBAH, $id), 200);   
});	

$app->get('/pelanggan/load', function (Request $request, Response $response, array $args) {
	// $db = $this->db;	
	// $orderBy = $request->getQueryParam("order_by");
	// $limit   = $request->getQueryParam("limit");
	// $offset  = $request->getQueryParam("offset");

	// $filter = "";

	// if (!empty($orderBy)){
	// 	$filter .= " ORDER BY $orderBy ";
	// }

	// if (!empty($limit)){
	// 	$filter .= " LIMIT $limit ";
	// }

	// if (!empty($offset)){
	// 	$filter .= " OFFSET $offset ";
	// }

	// try {
	// 	$sql = 	"SELECT id, kode, judul, isi, foto, user_id, tgl_non_aktif ".
	// 			"FROM berita $filter ";				
	// 	$query = $db->prepare($sql); 
	// 	$result = $query->execute();
	// 	$hasil = [];
	// 	if ($result) {		
	// 		$data = $query->fetchAll();
	// 		$hasil = setHasil(STATUS_SUKSES, $data);		
	// 	}else{
	// 		$data = $query->fetchAll();
	// 		$hasil = setHasil(STATUS_GAGAL, $data);		
	// 	}	
	// } catch(PDOException $pdoe) {
	// 	$hasil = setHasil(STATUS_GAGAL, $data);
	// }
		
  	// return $response->withJson($hasil);
});

$app->get('/pelanggan/get', function (Request $request, Response $response, array $args) {
	$db = $this->db;	
	$id = $request->getQueryParam("id");

	try {
		$sql = 	"SELECT id, email, no_telpon, DATE_FORMAT(tgl_lahir, '%d-%m-%Y') AS tgl_lahir, pswd, nama, jenis_kelamin, tipe_id, nomor_id, user_id, tgl_aktif ".
                "FROM master_pelanggan ".
                "WHERE id = $id ";
		$query = $db->prepare($sql); 
		$result = $query->execute();
		$hasil = [];
		if ($result) {		
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_SUKSES, $data);
		}else{
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_GAGAL, $data);		
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil);
});


$app->get('/pelanggan/masuk', function (Request $request, Response $response, array $args) {
    $db = $this->db;	
    
    $email_telpon = $request->getQueryParam("email_telpon");
	$password     = $request->getQueryParam("password");
	$id = 0;

    if (empty($email_telpon) && ($email_telpon == "")){ 
        return $response->withJson(setInfo(STATUS_GAGAL, "Email atau No. Telpon belum diisi", $id), 200);  
    }
    
    if (empty($password) && ($password == "")){ 
        return $response->withJson(setInfo(STATUS_GAGAL, "Password belum diisi", $id), 200);  
	}
    
	try {
        if (get_count($db, "master_pelanggan","((UPPER(email) = UPPER('$email_telpon')) OR (no_telpon = UPPER('$email_telpon'))) AND tgl_aktif IS NOT NULL") <= 0) {
            return $response->withJson(setInfo(STATUS_GAGAL, "Email belum terdaftar", $id), 200);
        }

		$sql = 	"SELECT id, email, no_telpon, pswd, nama, jenis_kelamin, tipe_id, nomor_id, user_id, tgl_aktif ".
                "FROM master_pelanggan ".
                "WHERE ((UPPER(email) = UPPER('$email_telpon')) OR (UPPER(no_telpon) = UPPER('$email_telpon'))) AND pswd = '$password' AND tgl_aktif IS NOT NULL";                
		$query = $db->prepare($sql); 
        $query->execute();
        $rowCount = $query->rowCount();
        if ($rowCount > 0){                  
            $hasil = [];
            $data = $query->fetchAll();
            $hasil = setHasil(STATUS_SUKSES, $data);
            return $response->withJson($hasil);
        }else{
            return $response->withJson(setInfo(STATUS_GAGAL, "Email atau password salah",$id), 200);
        }
	} catch(PDOException $pdoe) {
		return $response->withJson(setInfo(PESAN_GAGAL_KESALAHAN, "", $id), 200);
	}
});


$app->post('/pelanggan/ubah_password', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

        $id         = $dtPost['id'];        
        $pswd_lama  = $dtPost['pswd_lama'];
        $pswd_baru  = $dtPost['pswd_baru'];        
        
        if (empty($pswd_lama) && ($pswd_lama == "")){ 
            return $response->withJson(setInfo(STATUS_GAGAL, "Password lama belum diisi", $id), 200);
        }

        if (empty($pswd_baru) && ($pswd_baru == "")){ 
            return $response->withJson(setInfo(STATUS_GAGAL, "Password baru belum diisi", $id), 200);
        }

        if (get_count($db, "master_pelanggan","pswd = '$pswd_lama' AND id = $id") <= 0) {
            return $response->withJson(setInfo(STATUS_GAGAL, "Password lama tidak sesuai", $id), 200);
        }
		
		$sql =  "UPDATE master_pelanggan SET ".
					"pswd = :pswd ".					
				"WHERE id = :id";
		$query = $db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':pswd', $pswd_baru);
        $result = $query->execute();        
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_UBAH, $id), 200);   
});

$app->post('/pelanggan/atur_ulang_password', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

        $email      = $dtPost['email'];
		$pswd_baru  = $dtPost['pswd_baru']; 
		
		if (empty($email) && ($email == "")){ 
            return $response->withJson(setInfo(STATUS_GAGAL, "Email belum diisi", $id), 200);
        }

		if (empty($pswd_baru) && ($pswd_baru == "")){ 
            return $response->withJson(setInfo(STATUS_GAGAL, "Password baru belum diisi", $id), 200);
		}		
        
        if (get_count($db, "master_pelanggan","(UPPER(email) = UPPER('$email'))") <= 0) {
			return $response->withJson(setInfo(STATUS_GAGAL, "Email tidak terdaftar", $id), 200);
		}

		$id = get_field($db,"id","master_pelanggan"," id", "(UPPER(email) = UPPER('$email'))");
		
		$sql =  "UPDATE master_pelanggan SET ".
					"pswd = :pswd ".					
				"WHERE email = :email";
		$query = $db->prepare($sql);
		$query->bindParam(':email', $email);
		$query->bindParam(':pswd', $pswd_baru);
        $result = $query->execute();
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_UBAH, $id), 200);   
});

$app->post('/pelanggan/lupa_password', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

        $email      = $dtPost['email'];
		
		if (empty($email) && ($email == "")){ 
            return $response->withJson(setInfo(STATUS_GAGAL, "Email belum diisi", $id), 200);
        }
        
        if (get_count($db, "master_pelanggan","(UPPER(email) = UPPER('$email'))") <= 0) {
			return $response->withJson(setInfo(STATUS_GAGAL, "Email tidak terdaftar", $id), 200);
		}

		$id = get_field($db,"id","master_pelanggan"," id", "(UPPER(email) = UPPER('$email'))");
		$idPelanggan = $id;

		$token = RandomToken(32);
		$token = substr($token, 0, 31);
		
		$sql =  "INSERT INTO token_aktivasi (id_pelanggan, token) ".
                "VALUES(:id_pelanggan, :token)";
		$query = $db->prepare($sql);
		$query->bindParam(':id_pelanggan', $id);
        $query->bindParam(':token', $token);
		$query->execute();       
		$id_token = $db->lastInsertId();
		        
		$db->commit();  

		send_mail_reset_link($this->get('mail_options'),$email,$token,$idPelanggan);

	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_KESALAHAN, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_KESALAHAN, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, 'Link untuk mengubah password telah di kirim ke email anda', $id), 200);   
});


function RandomToken($length = 32){
    if(!isset($length) || intval($length) <= 8 ){
      $length = 32;
    }
    if (function_exists('random_bytes')) {
        return bin2hex(random_bytes($length));
    }
    if (function_exists('mcrypt_create_iv')) {
        return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
    } 
    if (function_exists('openssl_random_pseudo_bytes')) {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}