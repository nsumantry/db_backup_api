<?php
use Slim\Http\Request;
use Slim\Http\Response;
require_once __DIR__ . '/../../src/general/function_general.php';
require_once __DIR__ . '/../../src/general/const_global.php';
require_once __DIR__ . '/../../src/general/engine_general.php';

$app->get('/token_aktivasi/aktivasi', function (Request $request, Response $response, array $args) {
	$db = $this->db;	
    $id_pelanggan = $request->getQueryParam("id_pelanggan");
    $token        = $request->getQueryParam("token");
    
	try {
        if (get_count($db, "token_aktivasi","token = '$token' AND id_pelanggan = $id_pelanggan ") <= 0) {
            return $response->withJson(setInfo(STATUS_GAGAL, "Token tidak tersedia",0), 200);
        }

		$sql = 	"UPDATE master_pelanggan SET tgl_aktif = NOW() ".
                "WHERE id = $id_pelanggan ";
		$query = $db->prepare($sql); 
        $result = $query->execute();
        
        $sql = 	"DELETE FROM token_aktivasi ".
                "WHERE token = '$token' ";
        $query = $db->prepare($sql); 
        $result = $query->execute();
        
    } catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id_pelanggan), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id_pelanggan), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_SIMPAN, $id_pelanggan), 200);
});