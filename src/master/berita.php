<?php
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

require_once __DIR__ . '/../../src/general/function_general.php';
require_once __DIR__ . '/../../src/general/const_global.php';

$container = $app->getContainer();
$container['upload_directory'] = realpath(__DIR__ . '/../../media');
function moveUploadedFile($directory, UploadedFile $uploadedFile)
{
    $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
    $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
    $filename = sprintf('%s.%0.8s', $basename, $extension);

    $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

    return $filename;
}

$app->post('/berita/save', function (Request $request, Response $response) {	
	$db = $this->db;
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

		$tanggal = $dtPost['tanggal'];
        $kode    = $dtPost['kode'];
        $judul   = $dtPost['judul'];
        $isi     = $dtPost['isi'];
        $foto    = $dtPost['foto'];
		$user_id = $dtPost['user_id'];
		
		if (get_count($db, "berita","(UPPER(kode) = UPPER('$kode'))") > 0) {
            return $response->withJson(setInfo(STATUS_GAGAL, "Kode sudah ada",0), 200);
        }
        
        $sql =  "INSERT INTO berita (tanggal, kode, judul, isi, foto, user_id) ".
                "VALUES(:tanggal, :kode, :judul, :isi, :foto, :user_id)";
		$query = $db->prepare($sql);
		$query->bindParam(':tanggal', $tanggal);
		$query->bindParam(':kode', $kode);
		$query->bindParam(':judul', $judul);
		$query->bindParam(':isi', $isi);
		$query->bindParam(':foto', $foto);
        $query->bindParam(':user_id', $user_id);        
		$query->execute();       
		$id = $db->lastInsertId();
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_SIMPAN, $id), 200);   
});

$app->post('/berita/edit', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost  = $request->getParsedBody();
		$id 	 = $dtPost['id'];
		$tanggal = $dtPost['tanggal'];
        $kode    = $dtPost['kode'];
        $judul   = $dtPost['judul'];
        $isi     = $dtPost['isi'];
        $foto    = $dtPost['foto'];
        $user_id = $dtPost['user_id'];
		
		$sql =  "UPDATE berita SET ".
					"tanggal = :tanggal, ".
					"kode = :kode, ".
					"judul = :judul, ".
					"isi = :isi, ".
					"foto = :foto, ". 
					"user_id = :user_id ".
				"WHERE id = :id ";
		$query = $db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':tanggal', $tanggal);
		$query->bindParam(':kode', $kode);
		$query->bindParam(':judul', $judul);
		$query->bindParam(':isi', $isi);
		$query->bindParam(':foto', $foto);
        $query->bindParam(':user_id', $user_id);        
		$query->execute();       
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $pdoe), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $e), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_UBAH, $id), 200);   
});	
$app->post('/berita/upload_only', function (Request $request, Response $response) {	
	$filename = '';
	$id = 0;
	$field = 'foto';
	try{
		$dtPost   = $request->getParsedBody();
		$field    = $dtPost['field'];
		$directory = $this->get('upload_directory');
		$uploadedFiles = $request->getUploadedFiles();

		$uploadedFile = $uploadedFiles[$field];
		if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
			$filename = moveUploadedFile($directory, $uploadedFile);
		}
    }catch(Exception $e) {      
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, [$field=>$filename]), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_UBAH, [$field=>$filename]), 200);   
});

$app->post('/berita/uploadfoto', function (Request $request, Response $response) {	
	$db = $this->db;    
	$filename = '';
	$id = 0;
	try{
		$directory = $this->get('upload_directory');
		$uploadedFiles = $request->getUploadedFiles();
		// handle single input with single file upload
		$uploadedFile = $uploadedFiles['foto'];
		if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
			$filename = moveUploadedFile($directory, $uploadedFile);
    	}

		$db->beginTransaction();
        $dtPost = $request->getParsedBody();
		$id 	 = $dtPost['id'];
        $foto    = $filename;
		$sql =  "UPDATE berita SET ".
					"foto = :foto  ". 
				"WHERE id = :id";
		$query = $db->prepare($sql);
		$query->bindParam(':foto', $foto);
		$query->bindParam(':id', $id);
		$query->execute();  
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_UBAH, $id), 200);   
});

$app->post('/berita/aktivasi', function (Request $request, Response $response) {	
	$db = $this->db;
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();

		$id 	 	   = $dtPost['id'];
		$tgl_non_aktif = $dtPost['tgl_non_aktif'];        
		if ($tgl_non_aktif == ""){
			$tgl_non_aktif = null;
		}
		
		$sql =  "UPDATE berita SET ".					
					"tgl_non_aktif= :tgl_non_aktif ".
				"WHERE id = :id";
		$query = $db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':tgl_non_aktif', $tgl_non_aktif);		
		$query->execute();       
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_AKTIVASI, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_AKTIVASI, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_AKTIVASI, $id), 200);   
});


$app->get('/berita/load', function (Request $request, Response $response, array $args) {
	$db = $this->db;	
	$is_aktif = $request->getQueryParam("is_aktif");
	$orderBy  = $request->getQueryParam("order_by");
	$limit    = $request->getQueryParam("limit");
	$offset   = $request->getQueryParam("offset");	

	$filter  = "";
	$filter2 = "";

	if (!empty($is_aktif)){
		if ($is_aktif == "T"){
			$filter  .= " AND tgl_non_aktif IS NULL ";		
		}else if ($is_aktif == "F"){
			$filter  .= " AND tgl_non_aktif IS NOT NULL ";			
		}
	}

	if (!empty($orderBy)){
		$filter2 .= " ORDER BY $orderBy ";
	}

	if (!empty($limit)){
		$filter2 .= " LIMIT $limit ";
	}

	if (!empty($offset)){
		$filter2 .= " OFFSET $offset ";
	}	
	
	$data  = [];
	$hasil = [];
	try {		
		$jml_data = get_count($db, "berita"," id <> 0 $filter");

		$tanggal  = format_date_time_sql("tanggal"). " AS tanggal_2 ";
		$sql = 	"SELECT id, $tanggal, tanggal, kode, judul, isi, foto, user_id, tgl_non_aktif, $jml_data AS jml_data, ".
				"CONCAT(\"http://".$_SERVER['SERVER_NAME']."/e_tiket_api/media/\", foto ) AS foto_url ".
				"FROM berita WHERE id <> 0 $filter $filter2 ";
		$query = $db->prepare($sql); 
		$result = $query->execute();		
		
		if ($result) {		
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_SUKSES, $data);		
		}else{
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_GAGAL, $data);		
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}		
  	return $response->withJson($hasil);
});

$app->get('/berita/get', function (Request $request, Response $response, array $args) {
	$db = $this->db;
	$id = $request->getQueryParam("id");

	try {
		$tanggal  = format_date_time_sql("tanggal"). " AS tanggal_2 ";
		$sql = 	"SELECT id, $tanggal, tanggal, kode, judul, isi, foto, user_id, tgl_non_aktif, ".
				"CONCAT(\"http://".$_SERVER['SERVER_NAME']."/e_tiket_api/media/\", foto ) AS foto_url ".
				"FROM berita ".
				"WHERE id = $id ";
		$query = $db->prepare($sql); 
		$result = $query->execute();
		$hasil = [];
		if ($result) {		
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_SUKSES, $data);		
		}else{
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_GAGAL, $data);		
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil);
});