<?php

function setInfo($status, $pesan, $id){
    $hasil = [
        "status" => $status,
        "pesan"  => $pesan,
        "id"  => $id
    ];
    return $hasil;
}

function setHasil($status, $data){
    $hasil = [
        "status" => $status,
        "data"  => $data
    ];
    return $hasil;
}

function setHasil2($status, $data, $detail){
    $hasil = [
        "status" => $status,
        "data"  => $data,
        "detail"  => $detail
    ];
    return $hasil;
}

function setHasil3($status, $data, $detail, $pembayaran){
    $hasil = [
        "status" => $status,
        "data"  => $data,
        "detail"  => $detail,
        "pembayaran"  => $pembayaran
    ];
    return $hasil;
}

