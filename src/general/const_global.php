<?php
define("STATUS_SUKSES", "berhasil");
define("STATUS_GAGAL", "gagal");

define("PESAN_BERHASIL_SIMPAN", "Berhasil menyimpan");
define("PESAN_BERHASIL_UBAH", "Berhasil diubah");
define("PESAN_BERHASIL_AKTIVASI", "Berhasil aktivasi");
define("PESAN_BERHASIL_HAPUS", "Berhasil hapus");

define("PESAN_GAGAL_SIMPAN", "Gagal menyimpan");
define("PESAN_GAGAL_UBAH", "Gagal diubah");
define("PESAN_GAGAL_AKTIVASI", "Gagal aktivasi");
define("PESAN_GAGAL_KESALAHAN", "Terjadi kesalahan");
define("PESAN_GAGAL_HAPUS", "Gagal hapus");
