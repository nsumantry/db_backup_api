<?php

function get_count($db, $nama_tabel, $kondisi){    
    $sql = 	"SELECT COUNT(*) AS jml FROM $nama_tabel WHERE $kondisi ";    
    $query = $db->prepare($sql);     
    $result = $query->execute();
    if ($result) {		
        $data = $query->fetch();        
        return $data["jml"]; 
    }
    return 0;
}

function get_field($db, $field, $nama_tabel, $kondisi){    
  $sql = 	"SELECT $field AS field FROM $nama_tabel WHERE $kondisi ";    
  $query = $db->prepare($sql);     
  $result = $query->execute();
  if ($result) {
    $rowCount = $query->rowCount();
    if ($rowCount > 0){
        $data = $query->fetch();        
        return $data["field"];
    }
  }
  return "";
}
  


function format_date_time_sql($tanggal){
    $hasil = "
    CONCAT(
        CASE DAYOFWEEK($tanggal)
          WHEN 1 THEN 'Minggu'
          WHEN 2 THEN 'Senin'
          WHEN 3 THEN 'Selasa'
          WHEN 4 THEN 'Rabu'
          WHEN 5 THEN 'Kamis'
          WHEN 6 THEN 'Jumat'
          WHEN 7 THEN 'Sabtu'
        END,' ',
        DAY($tanggal),' ',
        CASE MONTH($tanggal) 
          WHEN 1 THEN 'Januari' 
          WHEN 2 THEN 'Februari' 
          WHEN 3 THEN 'Maret' 
          WHEN 4 THEN 'April' 
          WHEN 5 THEN 'Mei' 
          WHEN 6 THEN 'Juni' 
          WHEN 7 THEN 'Juli' 
          WHEN 8 THEN 'Agustus' 
          WHEN 9 THEN 'September'
          WHEN 10 THEN 'Oktober' 
          WHEN 11 THEN 'November' 
          WHEN 12 THEN 'Desember' 
        END,' ',
        YEAR($tanggal),' ',
        DATE_FORMAT($tanggal, '%H:%i')
      )";
    return $hasil;
}

function format_date_time_sql2($tanggal){
  $hasil = "
  CONCAT(
      DAY($tanggal),' ',
      CASE MONTH($tanggal) 
        WHEN 1 THEN 'Januari' 
        WHEN 2 THEN 'Februari' 
        WHEN 3 THEN 'Maret' 
        WHEN 4 THEN 'April' 
        WHEN 5 THEN 'Mei' 
        WHEN 6 THEN 'Juni' 
        WHEN 7 THEN 'Juli' 
        WHEN 8 THEN 'Agustus' 
        WHEN 9 THEN 'September'
        WHEN 10 THEN 'Oktober' 
        WHEN 11 THEN 'November' 
        WHEN 12 THEN 'Desember' 
      END,' ',
      YEAR($tanggal),' ',
      DATE_FORMAT($tanggal, '%H:%i')
    )";
  return $hasil;
}

function format_date_sql($tanggal){
  $hasil = "
  CONCAT(
      CASE DAYOFWEEK($tanggal)
        WHEN 1 THEN 'Minggu'
        WHEN 2 THEN 'Senin'
        WHEN 3 THEN 'Selasa'
        WHEN 4 THEN 'Rabu'
        WHEN 5 THEN 'Kamis'
        WHEN 6 THEN 'Jumat'
        WHEN 7 THEN 'Sabtu'
      END,' ',
      DAY($tanggal),' ',
      CASE MONTH($tanggal) 
        WHEN 1 THEN 'Januari' 
        WHEN 2 THEN 'Februari' 
        WHEN 3 THEN 'Maret' 
        WHEN 4 THEN 'April' 
        WHEN 5 THEN 'Mei' 
        WHEN 6 THEN 'Juni' 
        WHEN 7 THEN 'Juli' 
        WHEN 8 THEN 'Agustus' 
        WHEN 9 THEN 'September'
        WHEN 10 THEN 'Oktober' 
        WHEN 11 THEN 'November' 
        WHEN 12 THEN 'Desember' 
      END,' ',
      YEAR($tanggal)
    )";
  return $hasil;
}

function format_time_sql($tanggal){
  $hasil = "DATE_FORMAT($tanggal, '%H:%i')";
  return $hasil;
}

function format_date_sql3($tanggal){
  $hasil = "DATE_FORMAT($tanggal, '%d-%m-%Y')";
  return $hasil;
}


