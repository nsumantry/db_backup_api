<?php
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

require_once __DIR__ . '/../../src/general/function_general.php';
require_once __DIR__ . '/../../src/general/const_global.php';

$app->post('/jadwal_kapal/save', function (Request $request, Response $response) {	
	$db = $this->db;
	$id = 0;
	try {
		$db->beginTransaction();
		$dtPost = $request->getParsedBody();		

		$jadwal_sampai       = $dtPost['jadwal_sampai'];
        $tgl_berangkat       = $dtPost['tgl_berangkat'];  //2020-04-14 05:07:09
        $tgl_sampai          = $dtPost['tgl_sampai'];
        $id_kapal            = $dtPost['id_kapal'];
        $id_pelabuhan_asal   = $dtPost['id_pelabuhan_asal'];
        $id_pelabuhan_tujuan = $dtPost['id_pelabuhan_tujuan'];
        $harga_dewasa        = $dtPost['harga_dewasa'];
        $harga_anak          = $dtPost['harga_anak'];
        $jumlah_penumpang    = $dtPost['jumlah_penumpang'];
		$user_id             = $dtPost['user_id'];	
		$jadwal_kapal_detail = array_key_exists("jadwal_kapal_detail",$dtPost)?$dtPost['jadwal_kapal_detail']:"[]";

		$jml_jadwal = 0;
		$sql = 	"SELECT DATE('$jadwal_sampai') - DATE('$tgl_berangkat') AS jml_jadwal";
		$query = $db->prepare($sql);
		$query->execute();				
		$dataJml = $query->fetch();        
		$jml_jadwal = $dataJml["jml_jadwal"]+1;		
		
		$tgl_berangkat_tmp = $tgl_berangkat;
		$tgl_sampai_tmp    = $tgl_sampai;
		for ($i = 1; $i <= $jml_jadwal; $i++) {			
			$tgl_berangkat = "DATE_ADD('$tgl_berangkat_tmp', INTERVAL $i-1 DAY)";
			$tgl_sampai    = "DATE_ADD('$tgl_sampai_tmp', INTERVAL $i-1 DAY)";
			

			$sql =  "INSERT INTO jadwal_kapal (tgl_berangkat, tgl_sampai, id_kapal, id_pelabuhan_asal, id_pelabuhan_tujuan, harga_dewasa, harga_anak, jumlah_penumpang, tgl_input, user_id) ".
                	"VALUES($tgl_berangkat, $tgl_sampai, :id_kapal, :id_pelabuhan_asal, :id_pelabuhan_tujuan, :harga_dewasa, :harga_anak, :jumlah_penumpang, NOW(), :user_id)";
			$query = $db->prepare($sql);
			// $query->bindParam(':tgl_berangkat', $tgl_berangkat);
			// $query->bindParam(':tgl_sampai', $tgl_sampai);
			$query->bindParam(':id_kapal', $id_kapal);
			$query->bindParam(':id_pelabuhan_asal', $id_pelabuhan_asal);
			$query->bindParam(':id_pelabuhan_tujuan', $id_pelabuhan_tujuan);
			$query->bindParam(':harga_dewasa', $harga_dewasa);
			$query->bindParam(':harga_anak', $harga_anak);
			$query->bindParam(':jumlah_penumpang', $jumlah_penumpang);
			$query->bindParam(':user_id', $user_id);
			$query->execute();       
			$id = $db->lastInsertId();

			if ($jadwal_kapal_detail != []){
				if ($i == 1){
					$jadwal_kapal_detail = json_decode($jadwal_kapal_detail, true);
					foreach($jadwal_kapal_detail as $row) {
						$id_jadwal_transit = $row;
						$sql =  "INSERT INTO jadwal_kapal_detail (id_jadwal, id_jadwal_transit) ".
								"VALUES(:id_jadwal, :id_jadwal_transit)";
						$query = $db->prepare($sql);
						$query->bindParam(':id_jadwal', $id);
						$query->bindParam(':id_jadwal_transit', $id_jadwal_transit);
						$query->execute();
					}
				}else{					
					foreach($jadwal_kapal_detail as $row) {
						$id_jadwal_transit = $row;						
						$sql =	"SELECT id, tgl_berangkat, tgl_sampai, id_kapal, id_pelabuhan_asal, id_pelabuhan_tujuan, ".
								"harga_dewasa, harga_anak, jumlah_penumpang, tgl_input, user_id ".
								"FROM jadwal_kapal ".
								"WHERE id = $id_jadwal_transit";								
						$queryJ = $db->prepare($sql);						
						$queryJ->execute();						
						$dataJ = $queryJ->fetch();
						
						$j_tgl_berangkat       = $dataJ["tgl_berangkat"];
						$j_tgl_sampai          = $dataJ["tgl_sampai"];
						$j_id_kapal            = $dataJ["id_kapal"];  
						$j_id_pelabuhan_asal   = $dataJ["id_pelabuhan_asal"];
						$j_id_pelabuhan_tujuan = $dataJ["id_pelabuhan_tujuan"];
						$j_harga_dewasa        = $dataJ["harga_dewasa"];
						$j_harga_anak          = $dataJ["harga_anak"];   
						$j_jumlah_penumpang    = $dataJ["jumlah_penumpang"];												

						//Simpan jadwal transit baru
						$tgl_berangkat = "DATE_ADD('$j_tgl_berangkat', INTERVAL $i-1 DAY)";
						$tgl_sampai    = "DATE_ADD('$j_tgl_sampai', INTERVAL $i-1 DAY)";

						
						$sql =  "INSERT INTO jadwal_kapal (tgl_berangkat, tgl_sampai, id_kapal, id_pelabuhan_asal, id_pelabuhan_tujuan, harga_dewasa, harga_anak, jumlah_penumpang, tgl_input, user_id) ".
								"VALUES($tgl_berangkat, $tgl_sampai, :id_kapal, :id_pelabuhan_asal, :id_pelabuhan_tujuan, :harga_dewasa, :harga_anak, :jumlah_penumpang, NOW(), :user_id)";
						$query = $db->prepare($sql);						
						$query->bindParam(':id_kapal', $j_id_kapal);
						$query->bindParam(':id_pelabuhan_asal', $j_id_pelabuhan_asal);
						$query->bindParam(':id_pelabuhan_tujuan', $j_id_pelabuhan_tujuan);
						$query->bindParam(':harga_dewasa', $j_harga_dewasa);
						$query->bindParam(':harga_anak', $j_harga_anak);
						$query->bindParam(':jumlah_penumpang', $j_jumlah_penumpang);
						$query->bindParam(':user_id', $user_id);
						$query->execute(); 						$id_jadwal_transit_baru = $db->lastInsertId();
						
						
						$sql =  "INSERT INTO jadwal_kapal_detail (id_jadwal, id_jadwal_transit) ".
								"VALUES(:id_jadwal, :id_jadwal_transit)";
						$query = $db->prepare($sql);
						$query->bindParam(':id_jadwal', $id);
						$query->bindParam(':id_jadwal_transit', $id_jadwal_transit_baru);
						$query->execute();
					}
				}
			}			
		}    
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);  
    }catch(Exception $e) {      
	  $db->rollBack();	  
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_SIMPAN, $id), 200);   
});

$app->post('/jadwal_kapal/edit', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();
		$id 	             = $dtPost['id'];
        $tgl_berangkat       = $dtPost['tgl_berangkat'];  //2020-04-14 05:07:09
        $tgl_sampai          = $dtPost['tgl_sampai'];
        $id_kapal            = $dtPost['id_kapal'];
        $id_pelabuhan_asal   = $dtPost['id_pelabuhan_asal'];
        $id_pelabuhan_tujuan = $dtPost['id_pelabuhan_tujuan'];
        $harga_dewasa        = $dtPost['harga_dewasa'];
        $harga_anak          = $dtPost['harga_anak'];
        $jumlah_penumpang    = $dtPost['jumlah_penumpang'];
		$user_id             = $dtPost['user_id'];	
		$jadwal_kapal_detail = array_key_exists("jadwal_kapal_detail",$dtPost)?$dtPost['jadwal_kapal_detail']:"[]";
		
		$sql =  "UPDATE jadwal_kapal SET ".
                    "tgl_berangkat = :tgl_berangkat,".
                    "tgl_sampai = :tgl_sampai,".
                    "id_kapal = :id_kapal,".
                    "id_pelabuhan_asal = :id_pelabuhan_asal,".
                    "id_pelabuhan_tujuan = :id_pelabuhan_tujuan,".
                    "harga_dewasa = :harga_dewasa,".
                    "harga_anak = :harga_anak,".
                    "jumlah_penumpang = :jumlah_penumpang,".
                    "user_id = :user_id ".
				"WHERE id = :id";
		$query = $db->prepare($sql);
		$query->bindParam(':id', $id);
		$query->bindParam(':tgl_berangkat', $tgl_berangkat);
        $query->bindParam(':tgl_sampai', $tgl_sampai);
        $query->bindParam(':id_kapal', $id_kapal);
        $query->bindParam(':id_pelabuhan_asal', $id_pelabuhan_asal);
        $query->bindParam(':id_pelabuhan_tujuan', $id_pelabuhan_tujuan);
        $query->bindParam(':harga_dewasa', $harga_dewasa);
        $query->bindParam(':harga_anak', $harga_anak);
        $query->bindParam(':jumlah_penumpang', $jumlah_penumpang);
        $query->bindParam(':user_id', $user_id);
		$query->execute();

		$sql =  "DELETE FROM jadwal_kapal_detail WHERE id_jadwal = $id ";
        $query = $db->prepare($sql);
		$query->execute();

		$jadwal_kapal_detail = json_decode($jadwal_kapal_detail, true);
		foreach($jadwal_kapal_detail as $row) {
			$id_jadwal_transit = $row;
			$sql =  "INSERT INTO jadwal_kapal_detail (id_jadwal, id_jadwal_transit) ".
					"VALUES(:id_jadwal, :id_jadwal_transit)";
			$query = $db->prepare($sql);
			$query->bindParam(':id_jadwal', $id);
			$query->bindParam(':id_jadwal_transit', $id_jadwal_transit);
			$query->execute();
		}
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_UBAH, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_UBAH, $id), 200);   
});

$app->post('/jadwal_kapal/hapus', function (Request $request, Response $response) {	
	$db = $this->db;    
	$id = 0;
	try {
		$db->beginTransaction();
        $dtPost = $request->getParsedBody();
		$id = $dtPost['id'];       
		
		$sql =  "DELETE FROM jadwal_kapal ".
				"WHERE id = :id";
		$query = $db->prepare($sql);
		$query->bindParam(':id', $id);		
		$query->execute();

		$sql =  "DELETE FROM jadwal_kapal_detail WHERE id_jadwal = $id ";
        $query = $db->prepare($sql);
		$query->execute();
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_HAPUS, $id), 200);  
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_HAPUS, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_HAPUS, $id), 200);   
});

$app->get('/jadwal_kapal/load', function (Request $request, Response $response, array $args) {
    $db = $this->db;
    $tgl_berangkat = $request->getQueryParam("tgl_berangkat");
    $tgl_sampai    = $request->getQueryParam("tgl_sampai");
	$orderBy       = $request->getQueryParam("order_by");
	$limit         = $request->getQueryParam("limit");
	$offset        = $request->getQueryParam("offset");	
	$ids           = $request->getQueryParam("ids");	
	$id_kapal      = $request->getQueryParam("id_kapal");	

	$filter  = "";
	$filter2 = "";
	$filterIds = "";

	if (!empty($tgl_berangkat)){		
		$filter  .= " AND DATE(m.tgl_sampai) >= DATE('$tgl_berangkat') ";
    }
    
    if (!empty($tgl_sampai)){		
		$filter  .= " AND DATE(m.tgl_berangkat) <= DATE('$tgl_sampai') ";
	}

	if (!empty($orderBy)){
		$filter2 .= " ORDER BY $orderBy ";
	}

	if (!empty($limit)){
		$filter2 .= " LIMIT $limit ";
	}

	if (!empty($offset)){
		$filter2 .= " OFFSET $offset ";
	}
	
	if (!empty($ids)){
		$filterIds .= " AND  m.id IN ($ids) ";
	}
	
	if (!empty($id_kapal)){
		$filterIds .= " AND  k.id = $id_kapal ";
	}		
	
	$data  = [];
	$hasil = [];
	try {		
		$jml_data = get_count($db, "jadwal_kapal m"," id <> 0 $filter");

        $sql = 	"SELECT m.id, m.tgl_berangkat, m.tgl_sampai, m.id_kapal, m.id_pelabuhan_asal, m.id_pelabuhan_tujuan, m.harga_dewasa, ".
				"CONCAT(p1.nama,' - ',p2.nama,' : ',m.tgl_berangkat) AS display_transit,".
                "m.harga_anak, m.jumlah_penumpang, m.tgl_input, m.user_id, ".
                "k.kode AS kode_kapal, k.nama AS nama_kapal, ".
                "p1.kode AS kode_pelabuhan_asal, p1.nama AS nama_pelabuhan_asal, ".
                "p2.kode AS kode_pelabuhan_tujuan, p2.nama AS nama_pelabuhan_tujuan, ".
				"$jml_data AS jml_data ".
                "FROM jadwal_kapal m, master_kapal k, master_pelabuhan p1, master_pelabuhan p2 ".
                "WHERE m.id_kapal = k.id AND m.id_pelabuhan_asal = p1.id AND m.id_pelabuhan_tujuan = p2.id $filterIds $filter $filter2 ";                
		$query = $db->prepare($sql); 
		$result = $query->execute();		
		
		if ($result) {		
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_SUKSES, $data);		
		}else{
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_GAGAL, $data);		
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}		
  	return $response->withJson($hasil);
});


$app->get('/jadwal_kapal/cari', function (Request $request, Response $response, array $args) {
    $db = $this->db;
	$tanggal    		 = $request->getQueryParam("tanggal");
	$id_pelabuhan_asal   = $request->getQueryParam("id_pelabuhan_asal");
	$id_pelabuhan_tujuan = $request->getQueryParam("id_pelabuhan_tujuan");
	$orderBy    		 = $request->getQueryParam("order_by");

	$filter  = "";
    $filter2 = "";

	$filter = " AND DATE(m.tgl_berangkat) >= DATE('$tanggal') AND DATE('$tanggal') <= DATE(m.tgl_sampai) ";
	$filter .= " AND m.id_pelabuhan_asal = $id_pelabuhan_asal AND m.id_pelabuhan_tujuan = $id_pelabuhan_tujuan ";

	if (!empty($orderBy)){
		$filter2 .= " ORDER BY $orderBy ";
	}
	
	$data  = [];
	$hasil = [];
	try {		
		$jml_data = get_count($db, "jadwal_kapal m"," id <> 0 $filter");

		$sqlSisa =  "(IFNULL(".
						"(SELECT m.jumlah_penumpang - SUM(p1.jumlah_dewasa) - SUM(p1.jumlah_anak) ".
						"FROM pesanan p1 ".
						"WHERE p1.id_jadwal = m.id ".
						"OR p1.id IN (SELECT ps.id FROM pesanan ps, jadwal_kapal jk, jadwal_kapal_detail jkd ".
									 "WHERE ps.id_jadwal = jk.id AND jk.id = jkd.id_jadwal AND jkd.id_jadwal_transit = m.id )".
						")".
					", m.jumlah_penumpang)) AS sisa ";
		
		$durasi ="CONCAT(".
					"(CASE WHEN (FLOOR(HOUR(TIMEDIFF(tgl_sampai, tgl_berangkat)) / 24) > 0) THEN CONCAT(' ', FLOOR(HOUR(TIMEDIFF(tgl_sampai, tgl_berangkat)) / 24), ' hari') ELSE '' END), ".
					"(CASE WHEN (MOD(HOUR(TIMEDIFF(tgl_sampai, tgl_berangkat)), 24) > 0) THEN CONCAT(' ', MOD(HOUR(TIMEDIFF(tgl_sampai, tgl_berangkat)), 24), ' jam') ELSE '' END), ".
					"(CASE WHEN (MINUTE(TIMEDIFF(tgl_sampai, tgl_berangkat)) > 0) THEN CONCAT(' ', MINUTE(TIMEDIFF(tgl_sampai, tgl_berangkat)), ' menit') ELSE '' END) ".
				 ") AS durasi";

		$tgl_jam_berangkat = format_date_time_sql("tgl_berangkat")." AS tgl_jam_berangkat";
		$tgl_jam_sampai    = format_date_time_sql("tgl_sampai")." AS tgl_jam_sampai";
		$tgl_berangkat	   = format_date_sql("tgl_berangkat")." AS tgl_berangkat";
		$tgl_sampai    	   = format_date_sql("tgl_sampai")." AS tgl_sampai";
		$jam_berangkat	   = format_time_sql("tgl_berangkat")." AS jam_berangkat";
		$jam_sampai    	   = format_time_sql("tgl_sampai")." AS jam_sampai";		
		$sisa_waktu_pesan  = " ROUND((UNIX_TIMESTAMP(tgl_berangkat) - UNIX_TIMESTAMP(NOW())) / 60) AS sisa_waktu_pesan ";		

		$sql = 	"SELECT m.id, m.tgl_berangkat, m.tgl_sampai, m.id_kapal, m.id_pelabuhan_asal, m.id_pelabuhan_tujuan, m.harga_dewasa, ".
                "m.harga_anak, m.jumlah_penumpang, m.tgl_input, m.user_id, ".
                "k.kode AS kode_kapal, k.nama AS nama_kapal, ".
                "p1.kode AS kode_pelabuhan_asal, p1.nama AS nama_pelabuhan_asal, ".
				"p2.kode AS kode_pelabuhan_tujuan, p2.nama AS nama_pelabuhan_tujuan, ".
				"$durasi, $tgl_jam_berangkat, $tgl_jam_sampai , $tgl_berangkat, $tgl_sampai, $jam_berangkat, $jam_sampai, ".
				"$sqlSisa, ".
                "$jml_data AS jml_data, $sisa_waktu_pesan ".
                "FROM jadwal_kapal m, master_kapal k, master_pelabuhan p1, master_pelabuhan p2 ".
				"WHERE m.id_kapal = k.id AND m.id_pelabuhan_asal = p1.id AND m.id_pelabuhan_tujuan = p2.id $filter $filter2 ";
				//die($sql);
		$query = $db->prepare($sql);
		$result = $query->execute();
		if ($result) {		
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_SUKSES, $data);		
		}else{
			$data = $query->fetchAll();
			$hasil = setHasil(STATUS_GAGAL, $data);		
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}		
  	return $response->withJson($hasil);
});

$app->get('/jadwal_kapal/get', function (Request $request, Response $response, array $args) {
	$db = $this->db;
	$id = $request->getQueryParam("id");
	$data   = [];
	$detail = [];

	try {
		$durasi ="CONCAT(".
					"(CASE WHEN (FLOOR(HOUR(TIMEDIFF(tgl_sampai, tgl_berangkat)) / 24) > 0) THEN CONCAT(' ', FLOOR(HOUR(TIMEDIFF(tgl_sampai, tgl_berangkat)) / 24), ' hari') ELSE '' END), ".
					"(CASE WHEN (MOD(HOUR(TIMEDIFF(tgl_sampai, tgl_berangkat)), 24) > 0) THEN CONCAT(' ', MOD(HOUR(TIMEDIFF(tgl_sampai, tgl_berangkat)), 24), ' jam') ELSE '' END), ".
					"(CASE WHEN (MINUTE(TIMEDIFF(tgl_sampai, tgl_berangkat)) > 0) THEN CONCAT(' ', MINUTE(TIMEDIFF(tgl_sampai, tgl_berangkat)), ' menit') ELSE '' END) ".
				 ") AS durasi";

		$tgl_jam_berangkat = format_date_time_sql("tgl_berangkat")." AS tgl_jam_berangkat";
		$tgl_jam_sampai    = format_date_time_sql("tgl_sampai")." AS tgl_jam_sampai";
		$tgl_berangkat	   = format_date_sql("tgl_berangkat")." AS tgl_berangkat";
		$tgl_sampai    	   = format_date_sql("tgl_sampai")." AS tgl_sampai";
		$jam_berangkat	   = format_time_sql("tgl_berangkat")." AS jam_berangkat";
		$jam_sampai    	   = format_time_sql("tgl_sampai")." AS jam_sampai";

		$sql = 	"SELECT m.id, m.tgl_berangkat, m.tgl_sampai, m.id_kapal, m.id_pelabuhan_asal, m.id_pelabuhan_tujuan, m.harga_dewasa, ".
				"CONCAT(p1.nama,' - ',p2.nama,' : ',m.tgl_berangkat) AS display_transit,".
				"m.harga_anak, m.jumlah_penumpang, m.tgl_input, m.user_id, ".
                "k.kode AS kode_kapal, k.nama AS nama_kapal, ".
                "p1.kode AS kode_pelabuhan_asal, p1.nama AS nama_pelabuhan_asal, ".
				"p2.kode AS kode_pelabuhan_tujuan, p2.nama AS nama_pelabuhan_tujuan, ".
				"$durasi, $tgl_jam_berangkat, $tgl_jam_sampai , $tgl_berangkat, $tgl_sampai, $jam_berangkat, $jam_sampai ".
                "FROM jadwal_kapal m, master_kapal k, master_pelabuhan p1, master_pelabuhan p2 ".
				"WHERE m.id_kapal = k.id AND m.id_pelabuhan_asal = p1.id AND m.id_pelabuhan_tujuan = p2.id AND m.id = $id ";
		$query = $db->prepare($sql); 
		$result = $query->execute();

		$sql = 	"SELECT m.id, m.tgl_berangkat, m.tgl_sampai, m.id_kapal, m.id_pelabuhan_asal, m.id_pelabuhan_tujuan, m.harga_dewasa, ".
                "m.harga_anak, m.jumlah_penumpang, m.tgl_input, m.user_id, ".
                "k.kode AS kode_kapal, k.nama AS nama_kapal, ".
                "p1.kode AS kode_pelabuhan_asal, p1.nama AS nama_pelabuhan_asal, ".
				"p2.kode AS kode_pelabuhan_tujuan, p2.nama AS nama_pelabuhan_tujuan, ".
				"$durasi, $tgl_jam_berangkat, $tgl_jam_sampai , $tgl_berangkat, $tgl_sampai, $jam_berangkat, $jam_sampai ".
                "FROM jadwal_kapal m, master_kapal k, master_pelabuhan p1, master_pelabuhan p2 ".
				"WHERE m.id_kapal = k.id AND m.id_pelabuhan_asal = p1.id AND m.id_pelabuhan_tujuan = p2.id ".
				"AND m.id IN (SELECT jd.id_jadwal_transit FROM jadwal_kapal_detail jd WHERE jd.id_jadwal = $id ) ";
		$query2 = $db->prepare($sql); 
		$result2 = $query2->execute();
		
		$hasil = [];
		if ($result && $result2) {		
			$data   = $query->fetchAll();
			$detail = $query2->fetchAll();
			$jadwal_transit = [];
			foreach ($detail as $d) {
				array_push($jadwal_transit,$d['id']);
			};
			$data[0]['jadwal_kapal_detail'] = $jadwal_transit;
			$hasil  = setHasil2(STATUS_SUKSES, $data, $detail);
		}else{
			$data   = $query->fetchAll();
			$detail = $query2->fetchAll();
			$hasil  = setHasil2(STATUS_GAGAL, $data, $detail);
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil2(STATUS_GAGAL, $data, $detail);
	}
	
  	return $response->withJson($hasil);
});