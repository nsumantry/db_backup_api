<?php
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;


require_once __DIR__ . '/../../src/general/function_general.php';
require_once __DIR__ . '/../../src/general/const_global.php';

$app->post('/pesanan/save', function (Request $request, Response $response) {	
	$db = $this->db;
	$id = 0;
	try {
		$db->beginTransaction();
		$dtPost = $request->getParsedBody();        
        
        $id_pelanggan   = $dtPost['id_pelanggan'];
        $id_jadwal      = $dtPost['id_jadwal'];
        $jumlah_dewasa  = $dtPost['jumlah_dewasa'];
		$jumlah_anak    = $dtPost['jumlah_anak'];

		if (!empty($dtPost['pesanan_detail'])){
			$pesanan_detail = $dtPost['pesanan_detail'];
		}else{
			$pesanan_detail = [];
		}

		if (!empty($dtPost['pesanan_detail_nama'])){
			$pesanan_detail_nama = $dtPost['pesanan_detail_nama'];
		}else{
			$pesanan_detail_nama = [];
		}
		
		if (!empty($dtPost['nama'])){
			if ($dtPost['nama'] != ''){
				$nama = $dtPost['nama'];
			}else{
				$nama = get_field($db,"nama","master_pelanggan"," id = $id_pelanggan");
			}			
		}else{
			$nama = get_field($db,"nama","master_pelanggan"," id = $id_pelanggan");
		}
		
		$harga_dewasa = 0;
		$harga_anak	  = 0;
		$id_bayar	  = 0;
		$sql = 	"SELECT harga_dewasa, harga_anak, id_pelabuhan_asal ".
				"FROM jadwal_kapal ".
				"WHERE id = $id_jadwal ";
		$query = $db->prepare($sql);     
		$result = $query->execute();
		if ($result) {
			$data = $query->fetch();        
			$harga_dewasa = $data["harga_dewasa"];
			$harga_anak   = $data["harga_anak"];
			$id_pelabuhan_asal = $data["id_pelabuhan_asal"];
		}else{
			return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);
		}
		$total_harga = ($jumlah_dewasa * $harga_dewasa) + ($jumlah_anak * $harga_anak);

		$sql = "SELECT kode FROM master_pelabuhan WHERE id = $id_pelabuhan_asal";		
		$query  = $db->prepare($sql);
		$result = $query->execute();
		$dtPelabuhan   = $query->fetch();		
		$KodePelabuhan = $dtPelabuhan["kode"];

		$sqlNomor = "(SELECT CONCAT('$KodePelabuhan', DATE_FORMAT(NOW(), '%m%y'), LPAD((IFNULL(MAX(SUBSTR(nomor,LENGTH(nomor)-3, 4)),0)+1),4,'0')) ".
					"FROM pesanan p WHERE DATE_FORMAT(tanggal, '%m%y') = DATE_FORMAT(NOW(), '%m%y'))";		
        
        $sql =  "INSERT INTO pesanan (tanggal, nomor, nama, no_tiket, id_pelanggan, id_jadwal, harga_dewasa, harga_anak, jumlah_dewasa, jumlah_anak, total_harga, id_bayar, tgl_expaired) ".
                "VALUES(now(), $sqlNomor, :nama, '', :id_pelanggan, :id_jadwal, :harga_dewasa, :harga_anak, :jumlah_dewasa, :jumlah_anak, :total_harga, :id_bayar, DATE_ADD(NOW(), INTERVAL 2 HOUR))";
		$query = $db->prepare($sql);
		$query->bindParam(':nama', $nama);
        $query->bindParam(':id_pelanggan', $id_pelanggan);
        $query->bindParam(':id_jadwal', $id_jadwal);
        $query->bindParam(':harga_dewasa', $harga_dewasa);
        $query->bindParam(':harga_anak', $harga_anak);
        $query->bindParam(':jumlah_dewasa', $jumlah_dewasa);
        $query->bindParam(':jumlah_anak', $jumlah_anak);
		$query->bindParam(':total_harga', $total_harga);
		$query->bindParam(':id_bayar', $id_bayar);
		$query->execute();       
		$id = $db->lastInsertId();
		
		if ($pesanan_detail != []){
			$pesanan_detail = json_decode($pesanan_detail, true);
			foreach($pesanan_detail as $row) {
				$id_penumpang = $row;
				$nama = get_field($db,"nama","penumpang"," id = $id_penumpang");

				$sql =  "INSERT INTO pesanan_detail (id_pesanan, id_penumpang, nama) ".
						"VALUES(:id_pesanan, :id_penumpang, :nama)";
				$query = $db->prepare($sql);
				$query->bindParam(':nama', $nama);
				$query->bindParam(':id_pesanan', $id);
				$query->bindParam(':id_penumpang', $id_penumpang);
				$query->execute();
			}
		}	

		//Khusus untuk pembelian tiket langsung di pelabuhan
		if ($pesanan_detail_nama != []){
			$pesanan_detail_nama = json_decode($pesanan_detail_nama, true);
			foreach($pesanan_detail_nama as $row) {
				$id_penumpang = 1;			
				$nama = $row;
				$sql =  "INSERT INTO pesanan_detail (id_pesanan, id_penumpang, nama) ".
						"VALUES(:id_pesanan, :id_penumpang, :nama)";
				$query = $db->prepare($sql);
				$query->bindParam(':nama', $nama);
				$query->bindParam(':id_pesanan', $id);
				$query->bindParam(':id_penumpang', $id_penumpang);
				$query->execute();
			}
		}	
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $pdoe), 200);
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_SIMPAN, $id), 200);   
});

// $app->get('/pesanan/load', function (Request $request, Response $response, array $args) {
//     $db = $this->db;
//     $tgl_berangkat = $request->getQueryParam("tgl_berangkat");
//     $tgl_sampai    = $request->getQueryParam("tgl_sampai");
// 	$orderBy       = $request->getQueryParam("order_by");
// 	$limit         = $request->getQueryParam("limit");
// 	$offset        = $request->getQueryParam("offset");	

// 	$filter  = "";
// 	$filter2 = "";

// 	if (!empty($tgl_berangkat)){		
// 		$filter  .= " AND DATE(m.tgl_sampai) >= DATE('$tgl_berangkat') ";
//     }
    
//     if (!empty($tgl_sampai)){		
// 		$filter  .= " AND DATE(m.tgl_berangkat) <= DATE('$tgl_sampai') ";
// 	}

// 	if (!empty($orderBy)){
// 		$filter2 .= " ORDER BY $orderBy ";
// 	}

// 	if (!empty($limit)){
// 		$filter2 .= " LIMIT $limit ";
// 	}

// 	if (!empty($offset)){
// 		$filter2 .= " OFFSET $offset ";
// 	}	
	
// 	$data  = [];
// 	$hasil = [];
// 	try {		
// 		$jml_data = get_count($db, "pesanan m"," id <> 0 $filter");

//         $sql = 	"SELECT m.id, m.tgl_berangkat, m.tgl_sampai, m.id_kapal, m.id_pelabuhan_asal, m.id_pelabuhan_tujuan, m.harga_dewasa, ".
//                 "m.harga_anak, m.jumlah_penumpang, m.tgl_input, m.user_id, ".
//                 "k.kode AS kode_kapal, k.nama AS nama_kapal, ".
//                 "p1.kode AS kode_pelabuhan_asal, p1.nama AS nama_pelabuhan_asal, ".
//                 "p2.kode AS kode_pelabuhan_tujuan, p2.nama AS nama_pelabuhan_tujuan, ".
//                 "$jml_data AS jml_data ".
//                 "FROM pesanan m, master_kapal k, master_pelabuhan p1, master_pelabuhan p2 ".
//                 "WHERE m.id_kapal = k.id AND m.id_pelabuhan_asal = p1.id AND m.id_pelabuhan_tujuan = p2.id $filter $filter2 ";                
// 		$query = $db->prepare($sql); 
// 		$result = $query->execute();		
		
// 		if ($result) {		
// 			$data = $query->fetchAll();
// 			$hasil = setHasil(STATUS_SUKSES, $data);		
// 		}else{
// 			$data = $query->fetchAll();
// 			$hasil = setHasil(STATUS_GAGAL, $data);		
// 		}	
// 	} catch(PDOException $pdoe) {
// 		$hasil = setHasil(STATUS_GAGAL, $data);
// 	}		
//   	return $response->withJson($hasil);
// });

$app->get('/pesanan/get', function (Request $request, Response $response, array $args) {
	$db = $this->db;
	$id = $request->getQueryParam("id");
	$data = [];
	$detail = [];
	$pembayaran = [];
	try {
		$durasi ="CONCAT(".
				"(CASE WHEN (FLOOR(HOUR(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)) / 24) > 0) THEN CONCAT(' ', FLOOR(HOUR(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)) / 24), ' hari') ELSE '' END), ".
				"(CASE WHEN (MOD(HOUR(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)), 24) > 0) THEN CONCAT(' ', MOD(HOUR(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)), 24), ' jam') ELSE '' END), ".
				"(CASE WHEN (MINUTE(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)) > 0) THEN CONCAT(' ', MINUTE(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)), ' menit') ELSE '' END) ".
		") AS durasi";

		$status =   " CASE ".
						" WHEN m.id_bayar <= 0 AND j.tgl_sampai > now() AND m.tgl_expaired > now() THEN \"Belum Bayar\" ".
						" WHEN m.id_bayar <= 0 AND m.tgl_expaired <= now() THEN \"Kadaluarsa\" ".
						" WHEN m.id_bayar > 0 AND j.tgl_sampai > now() THEN \"Sudah Bayar\" ".
						" WHEN m.id_bayar > 0 AND j.tgl_sampai < now() THEN \"Selesai\" ".
					"END AS status , ".

		$tgl_jam_berangkat = format_date_time_sql("j.tgl_berangkat")." AS tgl_jam_berangkat";
		$tgl_jam_sampai    = format_date_time_sql("j.tgl_sampai")." AS tgl_jam_sampai";
		$tgl_berangkat     = format_date_sql("j.tgl_berangkat")." AS tgl_berangkat";
		$tgl_sampai        = format_date_sql("j.tgl_sampai")." AS tgl_sampai";
		$jam_berangkat     = format_time_sql("j.tgl_berangkat")." AS jam_berangkat";
		$jam_sampai        = format_time_sql("j.tgl_sampai")." AS jam_sampai";
		$tgl_bayar         = format_date_time_sql("m.tgl_bayar")." AS tgl_bayar";
		$tgl_expaired_2      = format_date_time_sql("m.tgl_expaired")." AS tgl_expaired_2";

		$sql = 	"SELECT m.id, m.tanggal, m.nomor, m.no_tiket, m.id_pelanggan, m.id_jadwal, m.harga_dewasa, m.harga_anak, m.jumlah_dewasa, ".
				"m.jumlah_anak, m.total_harga, m.id_bayar, $tgl_bayar, tgl_expaired, $tgl_expaired_2,".
				"j.tgl_berangkat, j.tgl_sampai, j.id_kapal, j.id_pelabuhan_asal, j.id_pelabuhan_tujuan, ".
                "k.kode AS kode_kapal, k.nama AS nama_kapal, ".
                "p1.kode AS kode_pelabuhan_asal, p1.nama AS nama_pelabuhan_asal, ".
				"p2.kode AS kode_pelabuhan_tujuan, p2.nama AS nama_pelabuhan_tujuan, ".
				"$durasi, $tgl_jam_berangkat, $tgl_jam_sampai , $tgl_berangkat, $tgl_sampai, $jam_berangkat, $jam_sampai, $status ".
                "FROM pesanan m, jadwal_kapal j, master_kapal k, master_pelabuhan p1, master_pelabuhan p2 ".
				"WHERE m.id_jadwal = j.id AND j.id_kapal = k.id AND j.id_pelabuhan_asal = p1.id AND j.id_pelabuhan_tujuan = p2.id AND m.id = $id";
		$query = $db->prepare($sql); 
		$result = $query->execute();

		$sql = 	"SELECT d.id AS id_penumpang, pm.nama, d.jenis_kelamin, d.tipe_id, '' AS jenis, d.nomor_id ".
				"FROM master_pelanggan d, pesanan pm ".
				"WHERE d.id = pm.id_pelanggan AND pm.id = $id ".
				"UNION ALL ".
				"SELECT d.id_penumpang, p.nama, p.jenis_kelamin, p.tipe_id, p.jenis, p.nomor_id ".
                "FROM pesanan_detail d, penumpang p ".
				"WHERE d.id_penumpang = p.id AND d.id_pesanan = $id";
		$query2 = $db->prepare($sql); 
		$result = $query2->execute();

		$sql = 	"SELECT id, kode, nama, jenis ".
				"FROM master_bayar ".
				"WHERE id = (SELECT pm.id_bayar FROM pesanan pm WHERE pm.id = $id) ";
		$query3 = $db->prepare($sql); 
		$result = $query3->execute();
		
		
		$hasil = [];
		if ($result) {		
			$data   = $query->fetchAll();
			$detail = $query2->fetchAll();
			$pembayaran = $query3->fetchAll();
			$hasil  = setHasil3(STATUS_SUKSES, $data, $detail, $pembayaran);
		}else{
			$data   = $query->fetchAll();
			$detail = $query2->fetchAll();
			$pembayaran = $query3->fetchAll();
			$hasil  = setHasil3(STATUS_GAGAL, $data, $detail, $pembayaran);
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil3(STATUS_GAGAL, $data, $detail, $pembayaran);
	}
		
  	return $response->withJson($hasil);
});


$app->get('/pesanan/load', function (Request $request, Response $response, array $args) {
	$db = $this->db;
	$id_pelanggan   = $request->getQueryParam("id_pelanggan");
	$orderBy  		= $request->getQueryParam("order_by");
	$limit    		= $request->getQueryParam("limit");
	$offset   		= $request->getQueryParam("offset");
	$is_sudah_bayar = $request->getQueryParam("is_sudah_bayar"); //diisi T untuk filter sudah dibayar atau F unutk belum bayar
	$is_selesai     = $request->getQueryParam("is_selesai"); //diisi T untuk filter sudah selesai (untuk riwayat) dan F untuk belum selesai (aktif/menunggu)

	//Keterangan
	//Aktif --> is_sudah_bayar = T dan is_selesai = F
	//Menunggu --> is_sudah_bayar = F dan is_selesai = F
	//Riwayat --> is_sudah_bayar = kosongkan dan is_selesai = T

	$filter  = "";
	$filter2 = "";

	if (!empty($id_pelanggan)){
		if ($id_pelanggan > 0){
			$filter .= " AND m.id_pelanggan = $id_pelanggan ";
		}
	}

	if (!empty($is_sudah_bayar)){
		if ($is_sudah_bayar == "T"){
			$filter .= " AND m.id_bayar > 0 ";
		}else if ($is_sudah_bayar == "F"){
			$filter .= " AND m.id_bayar <= 0 ";
		}
	}

	if (!empty($is_selesai)){
		if ($is_selesai == "T"){
			$filter .= " AND j.tgl_sampai < now() ";
		}else if ($is_selesai == "F"){
			$filter .= " AND j.tgl_sampai >= now() ";
		}	
	}

	if (!empty($orderBy)){
		$filter2 .= " ORDER BY $orderBy ";
	}

	if (!empty($limit)){
		$filter2 .= " LIMIT $limit ";
	}

	if (!empty($offset)){
		$filter2 .= " OFFSET $offset ";
	}

	$data = [];
	$jml_data = 0;
	try {		
		$sql = 	"SELECT COUNT(*) AS jml ".
                "FROM pesanan m, jadwal_kapal j, master_kapal k, master_pelabuhan p1, master_pelabuhan p2 ".
				"WHERE m.id_jadwal = j.id AND j.id_kapal = k.id AND j.id_pelabuhan_asal = p1.id AND j.id_pelabuhan_tujuan = p2.id $filter ";
		$query = $db->prepare($sql);     
		$result = $query->execute();
		if ($result) {		
			$count = $query->fetch();        
			$jml_data = $count["jml"]; 
		}

		$durasi ="CONCAT(".
				"(CASE WHEN (FLOOR(HOUR(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)) / 24) > 0) THEN CONCAT(' ', FLOOR(HOUR(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)) / 24), ' hari') ELSE '' END), ".
				"(CASE WHEN (MOD(HOUR(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)), 24) > 0) THEN CONCAT(' ', MOD(HOUR(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)), 24), ' jam') ELSE '' END), ".
				"(CASE WHEN (MINUTE(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)) > 0) THEN CONCAT(' ', MINUTE(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)), ' menit') ELSE '' END) ".
		") AS durasi";

		$status =   " CASE ".
						" WHEN m.id_bayar <= 0 AND j.tgl_sampai > now() AND m.tgl_expaired > now() THEN \"Belum Bayar\" ".
						" WHEN m.id_bayar <= 0 AND m.tgl_expaired <= now() THEN \"Kadaluarsa\" ".
						" WHEN m.id_bayar > 0 AND j.tgl_sampai > now() THEN \"Sudah Bayar\" ".
						" WHEN m.id_bayar > 0 AND j.tgl_sampai < now() THEN \"Selesai\" ".
					"END AS status , ".

		$tgl_jam_berangkat  = format_date_time_sql("j.tgl_berangkat")." AS tgl_jam_berangkat";
		$tgl_jam_sampai     = format_date_time_sql("j.tgl_sampai")." AS tgl_jam_sampai";
		$tgl_jam_berangkat2 = format_date_time_sql2("j.tgl_berangkat")." AS tgl_jam_berangkat_2";
		$tgl_jam_sampai2    = format_date_time_sql2("j.tgl_sampai")." AS tgl_jam_sampai_2";
		$tgl_berangkat      = format_date_sql("j.tgl_berangkat")." AS tgl_berangkat";
		$tgl_sampai         = format_date_sql("j.tgl_sampai")." AS tgl_sampai";
		$jam_berangkat      = format_time_sql("j.tgl_berangkat")." AS jam_berangkat";
		$jam_sampai         = format_time_sql("j.tgl_sampai")." AS jam_sampai";		

		$sql = 	"SELECT m.id, m.tanggal, m.nomor, m.no_tiket, m.id_pelanggan, m.id_jadwal, m.harga_dewasa, m.harga_anak, m.jumlah_dewasa, ".
				"m.jumlah_anak, m.total_harga, m.id_bayar, m.tgl_bayar, m.tgl_expaired,".
				"j.tgl_berangkat, j.tgl_sampai, j.id_kapal, j.id_pelabuhan_asal, j.id_pelabuhan_tujuan, ".
                "k.kode AS kode_kapal, k.nama AS nama_kapal, ".
                "p1.kode AS kode_pelabuhan_asal, p1.nama AS nama_pelabuhan_asal, ".
				"p2.kode AS kode_pelabuhan_tujuan, p2.nama AS nama_pelabuhan_tujuan, ".
				"$durasi, $tgl_jam_berangkat, $tgl_jam_sampai , $tgl_jam_berangkat2, $tgl_jam_sampai2, $tgl_berangkat, $tgl_sampai, $jam_berangkat, $jam_sampai, $status, $jml_data AS jml_data ".
                "FROM pesanan m, jadwal_kapal j, master_kapal k, master_pelabuhan p1, master_pelabuhan p2 ".
				"WHERE m.id_jadwal = j.id AND j.id_kapal = k.id AND j.id_pelabuhan_asal = p1.id AND j.id_pelabuhan_tujuan = p2.id $filter $filter2 ";
		$query = $db->prepare($sql); 
		$result = $query->execute();		
		$hasil = [];
		//die($sql);
		if ($result) {		
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_SUKSES, $data);
		}else{
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_GAGAL, $data);
		}
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil);
});

$app->post('/pesanan/delete', function (Request $request, Response $response) {	
	$db = $this->db;
	$id = 0;
	try {
		$db->beginTransaction();
		$dtPost = $request->getParsedBody();        
        
        $id  = $dtPost['id'];
        
        $sql =  "DELETE FROM pesanan WHERE id = $id ";
        $query = $db->prepare($sql);
		$query->execute();

		$sql =  "DELETE FROM pesanan_detail WHERE id_pesanan = $id ";
        $query = $db->prepare($sql);
		$query->execute();
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_SIMPAN, $id), 200);   
});


$app->post('/pesanan/bayar_pesanan', function (Request $request, Response $response) {	
	$db = $this->db;
	$id = 0;
	try {
		$db->beginTransaction();
		$dtPost = $request->getParsedBody();        
        
		$id   	  = $dtPost['id'];
		$id_bayar = $dtPost['id_bayar'];

		if (get_count($db, "pesanan"," now() > tgl_expaired AND id = $id  ") > 0) {
            return $response->withJson(setInfo(STATUS_GAGAL, "Tiket sudah kadaluarsa", $id), 200);
        }

		$sqlTiket = " (SELECT CONCAT(LPAD((IFNULL(MAX(SUBSTR(no_tiket,LENGTH(no_tiket), 5)),0)+1),5,'0')) ".
					" FROM (SELECT * FROM pesanan WHERE DATE_FORMAT(tanggal, '%m%y') = DATE_FORMAT(NOW(), '%m%y')) AS p), ";

		$sql =  "UPDATE pesanan set ".
					"no_tiket = $sqlTiket ".
					"id_bayar = $id_bayar, ".
					"tgl_bayar = NOW() ".
				"WHERE id = $id";
				
		$query = $db->prepare($sql);
		$query->execute();
		        
		$db->commit();  
	} catch(PDOException $pdoe) {
        $db->rollBack();		
		return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);
    }catch(Exception $e) {      
      $db->rollBack();
      return $response->withJson(setInfo(STATUS_GAGAL, PESAN_GAGAL_SIMPAN, $id), 200);  
	}	
  	return $response->withJson(setInfo(STATUS_SUKSES, PESAN_BERHASIL_SIMPAN, $id), 200);   
});

$app->get('/pesanan/cetak_tiket', function (Request $request, Response $response, array $args) {
	$db = $this->db;
	$id = $request->getQueryParam("id");
	$data = [];
	$detail = [];
	$pembayaran = [];
	try {
		$durasi ="CONCAT(".
				"(CASE WHEN (FLOOR(HOUR(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)) / 24) > 0) THEN CONCAT(' ', FLOOR(HOUR(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)) / 24), ' hari') ELSE '' END), ".
				"(CASE WHEN (MOD(HOUR(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)), 24) > 0) THEN CONCAT(' ', MOD(HOUR(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)), 24), ' jam') ELSE '' END), ".
				"(CASE WHEN (MINUTE(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)) > 0) THEN CONCAT(' ', MINUTE(TIMEDIFF(j.tgl_sampai, j.tgl_berangkat)), ' menit') ELSE '' END) ".
		") AS durasi";

		$status =   " CASE ".
						" WHEN m.id_bayar <= 0 AND j.tgl_sampai > now() THEN \"Belum Bayar\" ".
						" WHEN m.id_bayar <= 0 AND m.tgl_expaired <= now() THEN \"Kadaluarsa\" ".
						" WHEN m.id_bayar > 0 AND j.tgl_sampai > now() THEN \"Sudah Bayar\" ".
						" WHEN m.id_bayar > 0 AND j.tgl_sampai < now() THEN \"Selesai\" ".
					"END AS status , ".

		$tgl_jam_berangkat = format_date_time_sql("j.tgl_berangkat")." AS tgl_jam_berangkat";
		$tgl_jam_sampai    = format_date_time_sql("j.tgl_sampai")." AS tgl_jam_sampai";
		$tgl_berangkat     = format_date_sql("j.tgl_berangkat")." AS tgl_berangkat";
		$tgl_sampai        = format_date_sql("j.tgl_sampai")." AS tgl_sampai";
		$jam_berangkat     = format_time_sql("j.tgl_berangkat")." AS jam_berangkat";
		$jam_sampai        = format_time_sql("j.tgl_sampai")." AS jam_sampai";

		$sql = 	"SELECT m.id, m.tanggal, m.nomor, m.no_tiket, m.id_pelanggan, m.id_jadwal, m.harga_dewasa, m.harga_anak, m.jumlah_dewasa, ".
				"m.jumlah_anak, m.total_harga, m.id_bayar, m.tgl_bayar, m.tgl_expaired,".
				"j.tgl_berangkat, j.tgl_sampai, j.id_kapal, j.id_pelabuhan_asal, j.id_pelabuhan_tujuan, ".
                "k.kode AS kode_kapal, k.nama AS nama_kapal, ".
                "p1.kode AS kode_pelabuhan_asal, p1.nama AS nama_pelabuhan_asal, ".
				"p2.kode AS kode_pelabuhan_tujuan, p2.nama AS nama_pelabuhan_tujuan, ".
				"$durasi, $tgl_jam_berangkat, $tgl_jam_sampai , $tgl_berangkat, $tgl_sampai, $jam_berangkat, $jam_sampai, $status ".
                "FROM pesanan m, jadwal_kapal j, master_kapal k, master_pelabuhan p1, master_pelabuhan p2 ".
				"WHERE m.id_jadwal = j.id AND j.id_kapal = k.id AND j.id_pelabuhan_asal = p1.id AND j.id_pelabuhan_tujuan = p2.id AND m.id = $id";
		$query = $db->prepare($sql); 
		$result = $query->execute();

		$sql = 	"SELECT d.id AS id_penumpang, pm.nama, d.jenis_kelamin, d.tipe_id, '' AS jenis, d.nomor_id ".
				"FROM master_pelanggan d, pesanan pm ".
				"WHERE d.id = pm.id_pelanggan AND pm.id = $id ".
				"UNION ALL ".
				"SELECT d.id_penumpang, p.nama, p.jenis_kelamin, p.tipe_id, p.jenis, p.nomor_id ".
                "FROM pesanan_detail d, penumpang p ".
				"WHERE d.id_penumpang = p.id AND d.id_pesanan = $id";
		$query2 = $db->prepare($sql); 
		$result = $query2->execute();

		$sql = 	"SELECT id, kode, nama, jenis ".
				"FROM master_bayar ".
				"WHERE id = (SELECT pm.id_bayar FROM pesanan pm WHERE pm.id = $id) ";
		$query3 = $db->prepare($sql); 
		$result = $query3->execute();
		
		
		$hasil = [];
		if ($result) {		
			$data   = $query->fetchAll();
			$detail = $query2->fetchAll();
			$pembayaran = $query3->fetchAll();
			$hasil  = setHasil3(STATUS_SUKSES, $data, $detail, $pembayaran);
		}else{
			$data   = $query->fetchAll();
			$detail = $query2->fetchAll();
			$pembayaran = $query3->fetchAll();
			$hasil  = setHasil3(STATUS_GAGAL, $data, $detail, $pembayaran);
		}	
	} catch(PDOException $pdoe) {
		$hasil = setHasil3(STATUS_GAGAL, $data, $detail, $pembayaran);
	}
	if ($hasil){
		ob_start();
		$mpdf = new \Mpdf\Mpdf();
		$mpdf->WriteHTML($this->view->fetch('tiket.html', [
			'data' => $data[0], 'detail' => $detail, 'totalRp'=>number_format($data[0]['total_harga'], 0, ".", ","),
			]));
		ob_clean();
		$mpdf->Output('TIKET'.$data[0]['nomor'].'.pdf','I');
		// $response->write($pdf );
		// return $response;		
		// return $view;		
		$response = $this->response->withHeader('Content-Type','application/pdf');
		return $response;
	}	
  	// return $response->withJson($hasil);
});

$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig('twig/templates');

    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

    return $view;
};
