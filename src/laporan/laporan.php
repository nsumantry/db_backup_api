<?php
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

require_once __DIR__ . '/../../src/general/function_general.php';
require_once __DIR__ . '/../../src/general/const_global.php';

$app->get('/laporan/penjualan_tiket', function (Request $request, Response $response, array $args) {
    $db = $this->db;
    $tgl_berangkat_dari   = $request->getQueryParam("tgl_berangkat_dari");
    $tgl_berangkat_sampai = $request->getQueryParam("tgl_berangkat_sampai");
	$orderBy  		      = $request->getQueryParam("order_by");
	$limit    		      = $request->getQueryParam("limit");
	$offset   		      = $request->getQueryParam("offset");	

	$filter  = "";
    $filter2 = "";
    
    $filter .= " AND DATE(j.tgl_berangkat) BETWEEN  DATE('$tgl_berangkat_dari') AND DATE('$tgl_berangkat_sampai') ";

	if (!empty($orderBy)){
		$filter2 .= " ORDER BY $orderBy ";
	}

	if (!empty($limit)){
		$filter2 .= " LIMIT $limit ";
	}

	if (!empty($offset)){
		$filter2 .= " OFFSET $offset ";
	}

	$data = [];
	$jml_data = 0;
	try {
		$sql = 	"SELECT COUNT(*) AS jml ".
				"FROM jadwal_kapal j, master_kapal k, master_pelabuhan p1, master_pelabuhan p2 ".
				"WHERE j.id_kapal = k.id AND j.id_pelabuhan_asal = p1.id AND j.id_pelabuhan_tujuan = p2.id $filter";
		$query = $db->prepare($sql);     
		$result = $query->execute();
		if ($result) {		
			$count = $query->fetch();        
			$jml_data = $count["jml"]; 
		}

        $sqlDewasa = "(IFNULL(".
						"(SELECT SUM(p1.jumlah_dewasa)".
						"FROM pesanan p1 ".
						"WHERE (p1.id_jadwal = j.id ".
						"OR p1.id IN (SELECT ps.id FROM pesanan ps, jadwal_kapal jk, jadwal_kapal_detail jkd ".
									 "WHERE ps.id_jadwal = jk.id AND jk.id = jkd.id_jadwal AND jkd.id_jadwal_transit = j.id)) AND p1.id_bayar > 0 ".
						")".
                     ", 0)) AS terisi_dewasa ";
        
                    
        $sqlAnak =  "(IFNULL(".
                        "(SELECT SUM(p1.jumlah_anak)".
                        "FROM pesanan p1 ".
                        "WHERE (p1.id_jadwal = j.id ".
                        "OR p1.id IN (SELECT ps.id FROM pesanan ps, jadwal_kapal jk, jadwal_kapal_detail jkd ".
                                    "WHERE ps.id_jadwal = jk.id AND jk.id = jkd.id_jadwal AND jkd.id_jadwal_transit = j.id)) AND p1.id_bayar > 0 ".
                        ")".
                    ", 0)) AS terisi_anak ";

        //Total Harga Dewasa
        $sqlHrgDewasa = "(IFNULL(".
                            "(SELECT SUM(p1.jumlah_dewasa) * (p1.harga_dewasa)".
                            "FROM pesanan p1 ".
                            "WHERE (p1.id_jadwal = j.id ".
                            "OR p1.id IN (SELECT ps.id FROM pesanan ps, jadwal_kapal jk, jadwal_kapal_detail jkd ".
                                        "WHERE ps.id_jadwal = jk.id AND jk.id = jkd.id_jadwal AND jkd.id_jadwal_transit = j.id)) AND p1.id_bayar > 0 ".
                            ")".
                        ", 0)) AS total_harga_dewasa ";
        
        //Total Harga Anak
        $sqlHrgAnak  = "(IFNULL(".
                            "(SELECT SUM(p1.jumlah_anak) * (p1.harga_anak)".
                            "FROM pesanan p1 ".
                            "WHERE (p1.id_jadwal = j.id ".
                            "OR p1.id IN (SELECT ps.id FROM pesanan ps, jadwal_kapal jk, jadwal_kapal_detail jkd ".
                                        "WHERE ps.id_jadwal = jk.id AND jk.id = jkd.id_jadwal AND jkd.id_jadwal_transit = j.id)) AND p1.id_bayar > 0 ".
                            ")".
                        ", 0)) AS total_harga_anak ";


        $tgl_berangkat      = format_date_sql3("j.tgl_berangkat")." AS tgl_berangkat"; 
        $tgl_sampai         = format_date_sql3("j.tgl_sampai")." AS tgl_sampai"; 
		$tgl_jam_berangkat2 = format_date_sql("j.tgl_berangkat")." AS tgl_jam_berangkat_2"; //opsianl format saja
		$tgl_jam_sampai2    = format_date_sql("j.tgl_sampai")." AS tgl_jam_sampai_2";		//opsianl format saja

		$sql = 	"SELECT j.id, $tgl_berangkat, $tgl_sampai, $tgl_jam_berangkat2, $tgl_jam_sampai2, ".
                "k.kode AS kode_kapal, k.nama AS nama_kapal, ".
                "p1.kode AS kode_pelabuhan_asal, p1.nama AS nama_pelabuhan_asal, ".
				"p2.kode AS kode_pelabuhan_tujuan, p2.nama AS nama_pelabuhan_tujuan, j.harga_dewasa, j.harga_anak,".
				"$sqlDewasa, $sqlAnak, $sqlHrgDewasa, $sqlHrgAnak, $jml_data AS jml_data ".
                "FROM jadwal_kapal j, master_kapal k, master_pelabuhan p1, master_pelabuhan p2 ".
                "WHERE j.id_kapal = k.id AND j.id_pelabuhan_asal = p1.id AND j.id_pelabuhan_tujuan = p2.id $filter $filter2 ";
		$query = $db->prepare($sql); 
		$result = $query->execute();		
		$hasil = [];		
		if ($result) {		
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_SUKSES, $data);
		}else{
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_GAGAL, $data);
		}
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil);
});

$app->get('/laporan/keberangkatan_penumpang_kapal', function (Request $request, Response $response, array $args) {
    $db = $this->db;
    $tgl_berangkat_dari   = $request->getQueryParam("tgl_berangkat_dari");
    $tgl_berangkat_sampai = $request->getQueryParam("tgl_berangkat_sampai");
    $id_pelabuhan_asal    = $request->getQueryParam("id_pelabuhan_asal");
    $id_pelabuhan_tujuan  = $request->getQueryParam("id_pelabuhan_tujuan");    
	$orderBy  		      = $request->getQueryParam("order_by");
	$limit    		      = $request->getQueryParam("limit");
	$offset   		      = $request->getQueryParam("offset");	

	$filter  = "";
    $filter2 = "";

    $filter .= " AND DATE(j.tgl_berangkat) BETWEEN  DATE('$tgl_berangkat_dari') AND DATE('$tgl_berangkat_sampai') ";

    if (!empty($id_pelabuhan_asal) && ($id_pelabuhan_asal > 0)){
		$filter .= " AND j.id_pelabuhan_asal = $id_pelabuhan_asal ";
    }
    
    if (!empty($id_pelabuhan_tujuan) && ($id_pelabuhan_tujuan > 0)){
		$filter .= " AND j.id_pelabuhan_tujuan = $id_pelabuhan_tujuan ";
	}

	if (!empty($orderBy)){
		$filter2 .= " ORDER BY $orderBy ";
	}

	if (!empty($limit)){
		$filter2 .= " LIMIT $limit ";
	}

	if (!empty($offset)){
		$filter2 .= " OFFSET $offset ";
	}

	$data = [];
	$jml_data = 0;
	try {
		$sql = 	"SELECT SUM(jml) AS jml FROM (".
					"SELECT COUNT(*) AS jml ".
					"FROM pesanan m, jadwal_kapal j, master_kapal k, master_pelabuhan p1, master_pelabuhan p2, master_pelanggan mp ".
					"WHERE m.id_bayar > 0 AND m.id_pelanggan = mp.id AND m.id_jadwal = j.id AND j.id_kapal = k.id AND j.id_pelabuhan_asal = p1.id AND j.id_pelabuhan_tujuan = p2.id $filter ".

					" UNION ALL ".

					"SELECT COUNT(*) AS jml ".
					"FROM pesanan m, pesanan_detail pd, jadwal_kapal j, master_kapal k, master_pelabuhan p1, master_pelabuhan p2, penumpang mp ".
					"WHERE m.id = pd.id_pesanan AND m.id_bayar > 0 AND pd.id_penumpang = mp.id AND m.id_jadwal = j.id AND j.id_kapal = k.id AND j.id_pelabuhan_asal = p1.id AND j.id_pelabuhan_tujuan = p2.id $filter ".
				") AS data ";
		$query = $db->prepare($sql);     
		$result = $query->execute();
		if ($result) {		
			$count = $query->fetch();        
			$jml_data = $count["jml"]; 
		}

        $tanggal            = format_date_sql3("m.tanggal")." AS tanggal";
        $tgl_berangkat      = format_date_sql3("j.tgl_berangkat")." AS tgl_berangkat"; 
        $tgl_sampai         = format_date_sql3("j.tgl_sampai")." AS tgl_sampai"; 
		$tgl_jam_berangkat2 = format_date_sql("j.tgl_berangkat")." AS tgl_jam_berangkat_2"; //opsianl format saja
		$tgl_jam_sampai2    = format_date_sql("j.tgl_sampai")." AS tgl_jam_sampai_2";		//opsianl format saja

		$sql = 	"SELECT m.id, $tanggal, m.nomor, mp.nomor_id, m.nama AS nama_penumpang, ".
				"$tgl_berangkat, $tgl_sampai, $tgl_jam_berangkat2, $tgl_jam_sampai2, ".
                "k.kode AS kode_kapal, k.nama AS nama_kapal, ".
                "p1.kode AS kode_pelabuhan_asal, p1.nama AS nama_pelabuhan_asal, ".
				"p2.kode AS kode_pelabuhan_tujuan, p2.nama AS nama_pelabuhan_tujuan, $jml_data AS jml_data ".
                "FROM pesanan m, jadwal_kapal j, master_kapal k, master_pelabuhan p1, master_pelabuhan p2, master_pelanggan mp ".
                "WHERE m.id_bayar > 0 AND m.id_pelanggan = mp.id AND m.id_jadwal = j.id AND j.id_kapal = k.id AND j.id_pelabuhan_asal = p1.id AND j.id_pelabuhan_tujuan = p2.id $filter ".

                " UNION ALL ".

                "SELECT m.id, $tanggal , m.nomor, mp.nomor_id, m.nama AS nama_penumpang, ".
				"$tgl_berangkat, $tgl_sampai, $tgl_jam_berangkat2, $tgl_jam_sampai2, ".
                "k.kode AS kode_kapal, k.nama AS nama_kapal, ".
                "p1.kode AS kode_pelabuhan_asal, p1.nama AS nama_pelabuhan_asal, ".
				"p2.kode AS kode_pelabuhan_tujuan, p2.nama AS nama_pelabuhan_tujuan, $jml_data AS jml_data ".
                "FROM pesanan m, pesanan_detail pd, jadwal_kapal j, master_kapal k, master_pelabuhan p1, master_pelabuhan p2, penumpang mp ".
                "WHERE m.id = pd.id_pesanan AND m.id_bayar > 0 AND pd.id_penumpang = mp.id AND m.id_jadwal = j.id AND j.id_kapal = k.id AND j.id_pelabuhan_asal = p1.id AND j.id_pelabuhan_tujuan = p2.id $filter ".
                $filter2;                
		$query = $db->prepare($sql);
		$result = $query->execute();
		$hasil = [];
		if ($result) {
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_SUKSES, $data);
		}else{
			$data   = $query->fetchAll();
			$hasil  = setHasil(STATUS_GAGAL, $data);
		}
	} catch(PDOException $pdoe) {
		$hasil = setHasil(STATUS_GAGAL, $data);
	}
		
  	return $response->withJson($hasil);
});